//
//  GZCKitDemoTests.m
//  GZCKitDemoTests
//
//  Created by ZhongCheng Guo on 2018/3/29.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ViewController.h"

@interface GZCKitDemoTests : XCTestCase

/// 测试实例
@property (nonatomic,strong) ViewController *vc;

@end

@implementation GZCKitDemoTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    self.vc = [[ViewController alloc] init];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    self.vc = nil;
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    NSString *result = [self.vc setPBQuery:@"123"];
    
    XCTAssertEqual(result, @"123",@"测试不通过");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
        for (int i = 0 ; i < 100 ; i ++) {
            [self.vc setPBQuery:[NSString stringWithFormat:@"%d",i]];
        }
    }];
}

@end
