//
//  ViewController.m
//  GZCKitDemo
//
//  Created by ZhongCheng Guo on 2018/3/29.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//

#import "ViewController.h"
#import "XMLDictionary.h"
#import "DemoMessage.pbobjc.h"

#import "GZCLanguageCheckTool.h"

#import "NSString+GZCMatch.h"

#import "GZCLineTextField.h"

#import <objc/runtime.h>

typedef void(^ErgodicBlock)(NSDictionary *resultDic,NSArray *repeat,NSArray *oldKeys,NSDictionary *notesDic);
typedef void(^EncodeBlock)(NSArray *nofundKeys);

@interface ViewController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet GZCLineTextField *textfield;

    
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    
//    GZCLineTextField *textfield = [[GZCLineTextField alloc]initWithFrame:CGRectMake(100, 50, 200, 50)];
//    textfield.placeholder = @"1234";
//    [self.view addSubview:textfield];
//    self.textfield.placeholder = @"用户名";
//    self.textfield.lineHight = 2.f;
//    self.textfield.lineFocusDoubleHight = NO;
//    self.textfield.delegate = self;
//    self.textfield.contentEdgeInsets = UIEdgeInsetsMake(8,0,8,0);
//    self.textfield.clearButtonMode = UITextFieldViewModeNever;
//
//    UIView *leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
//    leftView.backgroundColor = [UIColor grayColor];
//    self.textfield.leftView = leftView;
//    self.textfield.leftViewMode = UITextFieldViewModeAlways;

//    UIView *rightView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
//    rightView.backgroundColor = [UIColor grayColor];
//    self.textfield.rightView = rightView;
//    self.textfield.rightViewMode = UITextFieldViewModeWhileEditing;
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(endEditing)];
    [self.view addGestureRecognizer:tap];
    
}
    
- (void)textFieldDidEndEditing:(UITextField *)textField{
    int suiji = arc4random()%10;
    if (suiji %2 == 0) {
        [self.textfield showErrorMessage:[NSString stringWithFormat:@"%@%d",@"错误提示",suiji] position:suiji%3];
    }else{
        [self.textfield showSuccessMessage:[NSString stringWithFormat:@"%@%d",@"成功提示",suiji] position:suiji%3];
    }
}

- (void)endEditing{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void)多语言{
//    NSString *filePath = [self XMLPathWithName:@"strings_zh"];
//    NSData *xmlData = [NSData dataWithContentsOfFile:filePath];
//    NSDictionary *zh_xml_dic = [NSDictionary dictionaryWithXMLData:xmlData];
//
//
//    filePath = [self XMLPathWithName:@"strings_ru"];
//    xmlData = [NSData dataWithContentsOfFile:filePath];
//    NSDictionary *ru_xml_dic = [NSDictionary dictionaryWithXMLData:xmlData];
//
//    NSDictionary *zh_ios = [GZCLanguageCheckTool decodeFile:[GZCLanguageCheckTool pathWithString:@"zh"] block:^(NSArray *repeat, NSArray *oldKeys, NSDictionary *notesDic) {
//    }];
//    NSDictionary *ru_ios = [GZCLanguageCheckTool decodeFile:[GZCLanguageCheckTool pathWithString:@"ru"] block:^(NSArray *repeat, NSArray *oldKeys, NSDictionary *notesDic) {
//    }];
//
//    NSDictionary *zh_android = [self xmlToMyDic:zh_xml_dic];
//    NSDictionary *ru_android = [self xmlToMyDic:ru_xml_dic];
//
//    NSDictionary *ru_android_fan = [self xmlToMyfanDic:ru_xml_dic];
//    NSDictionary *zh_android_fan = [self xmlToMyfanDic:zh_xml_dic];
//
//    NSMutableString *difTotalStr = [NSMutableString string];
//    [ru_ios enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
//        NSString *ru_android_key = ru_android_fan[obj];
//        if (ru_android_key == nil) {
//            NSString *zh_ios_value = zh_ios[key];
//            NSString *zh_android_key = zh_android_fan[zh_ios_value];
//            NSMutableString *difStr = [NSMutableString stringWithFormat:@"----------------------------------------\n"];
//            if (zh_android_key) {
//                [difStr appendFormat:@"key_android:%@\n",zh_android_key];
//                [difStr appendFormat:@"ru_android:%@\n",ru_android[zh_android_key]];
//                [difStr appendFormat:@"zh_android:%@\n",zh_android[zh_android_key]];
//            }else{
//                [difStr appendFormat:@"ru_android:未找到\n"];
//                [difStr appendFormat:@"zh_android:未找到\n"];
//            }
//            [difStr appendFormat:@"key_ios:%@\n",key];
//            [difStr appendFormat:@"ru_ios:%@\n",zh_ios[key]];
//            [difStr appendFormat:@"zh_ios:%@\n",obj];
//            [difTotalStr appendString:difStr];
//        }
//    }];
//
//    NSLog(@"%@",difTotalStr);
//    [GZCLanguageCheckTool writeString:difTotalStr toPath:@"/Users/guozhongcheng/Desktop/lun_change/比对结果.txt"];
//
//    // 对ProtocolBuffer的测试
//    DemoMessage *message = [DemoMessage new];
//    message.query = @"query";
//    NSLog(@"%@",message.query);
//
//    [GZCLanguageCheckTool automaticCheck];
//
//    NSDictionary *zhDic = [GZCLanguageCheckTool decodeFile:[GZCLanguageCheckTool pathWithString:kZH_Name] block:^(NSArray *repeat, NSArray *oldKeys, NSDictionary *notesDic) {
//    }];
//    NSDictionary *enDic = [GZCLanguageCheckTool decodeFile:[GZCLanguageCheckTool pathWithString:kEN_Name] block:^(NSArray *repeat, NSArray *oldKeys, NSDictionary *notesDic) {
//    }];
//    NSDictionary *jaDic = [GZCLanguageCheckTool decodeFile:[GZCLanguageCheckTool pathWithString:kJA_Name] block:^(NSArray *repeat, NSArray *oldKeys, NSDictionary *notesDic) {
//    }];
//    NSDictionary *koDic = [GZCLanguageCheckTool decodeFile:[GZCLanguageCheckTool pathWithString:kKO_Name] block:^(NSArray *repeat, NSArray *oldKeys, NSDictionary *notesDic) {
//    }];
//    NSDictionary *ruDic = [GZCLanguageCheckTool decodeFile:[GZCLanguageCheckTool pathWithString:kRU_Name] block:^(NSArray *repeat, NSArray *oldKeys, NSDictionary *notesDic) {
//    }];
//
//
//    [self writeDictionary:zhDic toPath:[NSString stringWithFormat:@"%@%@.json",kBaseOutPath,kZH_Name]];
//    [self writeDictionary:enDic toPath:[NSString stringWithFormat:@"%@%@.json",kBaseOutPath,kEN_Name]];
//    [self writeDictionary:jaDic toPath:[NSString stringWithFormat:@"%@%@.json",kBaseOutPath,kJA_Name]];
//    [self writeDictionary:koDic toPath:[NSString stringWithFormat:@"%@%@.json",kBaseOutPath,kKO_Name]];
//    [self writeDictionary:ruDic toPath:[NSString stringWithFormat:@"%@%@.json",kBaseOutPath,kRU_Name]];
//}

- (NSDictionary *)xmlToMyDic:(NSDictionary *)dic{
    NSArray *strings = dic[@"string"];
    NSMutableDictionary *mutDic = [NSMutableDictionary dictionary];
    NSMutableArray *repeat = [NSMutableArray array];
    [strings enumerateObjectsUsingBlock:^(NSDictionary *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *key = obj[@"_name"];
        NSString *value = obj[@"__text"];
        if (key == nil || value == nil) {
            value = obj[@"u"];
            if (value == nil) {
                return;
            }
        }
        if ([mutDic objectForKey:key]) {
            [repeat addObject:key];
        }else{
            if ([value isKindOfClass:[NSString class]]) {
                if ([value containsString:@"%s"]) {
                    value = [value stringByReplacingOccurrencesOfString:@"%s" withString:@"%@"];
                }
            }else{
                if ([value isKindOfClass:[NSArray class]]) {
                    NSMutableString *mut = [NSMutableString string];
                    [((NSArray *)value) enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        [mut appendString:obj];
                    }];
                    value = mut;
                }
            }
            [mutDic setObject:value forKey:key];
        }
    }];
    return mutDic;
}

- (NSDictionary *)xmlToMyfanDic:(NSDictionary *)dic{
    NSArray *strings = dic[@"string"];
    NSMutableDictionary *mutDic = [NSMutableDictionary dictionary];
    NSMutableArray *repeat = [NSMutableArray array];
    [strings enumerateObjectsUsingBlock:^(NSDictionary *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *key = obj[@"_name"];
        NSString *value = obj[@"__text"];
        if (key == nil || value == nil) {
            NSLog(@"%@",obj);
            value = obj[@"u"];
            if (value == nil) {
                NSLog(@"value %@",obj);
                return;
            }
        }
        if ([mutDic objectForKey:key]) {
            [repeat addObject:key];
        }else{
            if ([value isKindOfClass:[NSString class]]) {
                if ([value containsString:@"%s"]) {
                    value = [value stringByReplacingOccurrencesOfString:@"%s" withString:@"%@"];
                }
            }else{
                if ([value isKindOfClass:[NSArray class]]) {
                    NSMutableString *mut = [NSMutableString string];
                    [((NSArray *)value) enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        [mut appendString:obj];
                    }];
                    value = mut;
                }
            }
            [mutDic setObject:key forKey:value];
        }
    }];
    return mutDic;
}

- (NSString *)XMLPathWithName:(NSString *)name{
    return [NSString stringWithFormat:@"%@%@.xml",kBasePath,name];
}

@end
