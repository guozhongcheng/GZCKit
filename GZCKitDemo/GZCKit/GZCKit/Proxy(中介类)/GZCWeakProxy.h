//
//  GZCWeakProxy.h
//  GZCKit
//
//  Created by GuoZhongCheng on 2018/6/27.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//

/**
 中介类，用于防止某些类型的循环引用，如NSTimer，可以这样使用：
 
 ...
 @property (nonatomic,strong) NSTimer *timer;
 ...
 
 // 初始化timer时，使用Proxy防止循环引用
 self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:[GZCWeakProxy proxyWithTarget:self] selector:@selector(timerFire) userInfo:nil repeats:YES];
 ...
 */


#import <Foundation/Foundation.h>

@interface GZCWeakProxy : NSProxy

/// 回调方法的目标
@property ( nonatomic, weak, readonly ) id target;

/**
 初始化方法

 @param target 要调用的方法所在的对象
 @return Proxy对象
 */
+ (instancetype)proxyWithTarget:(id)target;
- (instancetype)initWithTarget:(id)target;

@end
