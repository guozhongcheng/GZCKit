//
//  GZCWeakProxy.m
//  GZCKit
//
//  Created by GuoZhongCheng on 2018/6/27.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//

#import "GZCWeakProxy.h"

@implementation GZCWeakProxy

- (instancetype)initWithTarget:(id)target{
    // NSProxy没有init方法，所以不需要调用super
    _target = target;
    return self;
}

+ (instancetype)proxyWithTarget:(id)target{
    return [[self alloc]initWithTarget:target];
}

// 进行消息转发，由于当前类中不存在target中所需要的方法，将其转发给实际的target进行处理
- (void)forwardInvocation:(NSInvocation *)invocation{
    SEL sel = [invocation selector];
    if ([self.target respondsToSelector:sel]) {
        [invocation invokeWithTarget:self.target];
    }
}

// 当一个SEL到来的时候，在这里返回SEL对应的NSMethodSignature
- (NSMethodSignature *)methodSignatureForSelector:(SEL)sel{
    return [self.target methodSignatureForSelector:sel];
}

// 是否响应SEL
- (BOOL)respondsToSelector:(SEL)aSelector{
    return [self.target respondsToSelector:aSelector];
}

@end
