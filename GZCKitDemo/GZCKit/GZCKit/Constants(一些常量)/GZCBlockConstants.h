//
//  GZCBlockConstants.h
//  GZCKitDemo
//
//  Created by GuoZhongCheng on 2018/7/30.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//
//  常用Block回调

#ifndef GZCBlockConstants_h
#define GZCBlockConstants_h

// 值回调
typedef void (^GZCbjectBlock) (id value);
typedef void (^GZCStringBlock) (NSString *value);
typedef void (^GZCIntegerBlock) (NSInteger value);
typedef void (^GZCFloatBlock) (CGFloat value);
typedef void (^GZCDictionaryBlock) (NSDictionary *value);
typedef void (^GZCArrayBlock) (NSArray *value);
typedef void (^GZCIndexPathBlock) (NSIndexPath *value);

typedef void (^GZCBlock) (void);
typedef void (^GZCErrorBlock) (NSError *error);

// 控件回调
typedef void (^GZCButtonBlock) (UIButton *button);

#endif /* GZCBlockConstants_h */
