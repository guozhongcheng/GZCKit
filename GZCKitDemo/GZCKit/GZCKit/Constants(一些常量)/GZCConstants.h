//
//  GZCConstants.h
//  GZCKit
//
//  Created by GuoZhongCheng on 2018/7/2.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//
//  常用常量

#ifndef GZCConstants_h
#define GZCConstants_h


#endif /* GZCConstants_h */

// 对于block的弱引用
#define _gzc_weakSelf() __weak typeof(self) weakSelf = self
// 强引用
#define _gzc_strongSelf() __strong typeof(weakSelf) strongSelf = weakSelf;
