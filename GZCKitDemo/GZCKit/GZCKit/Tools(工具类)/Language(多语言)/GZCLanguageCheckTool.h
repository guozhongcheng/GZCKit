//
//  GZCLanguageCheckTool.h
//  GZCKit
//
//  Created by GuoZhongCheng on 2018/7/30.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//
//  对多语言文件进行快速对比的工具类
//  使用时将对应多语言文件拷贝到任意路径的txt文件中，使用对应方法导出为txt文件。

#warning -- 语言原文件文件夹路径
#define kBasePath       @"/Users/guozhongcheng/Desktop/lun/"
#warning -- 转换后输出文件路径
#define kBaseOutPath    @"/Users/guozhongcheng/Desktop/lun_change/"
// 各语言对应文件名
#define kZH_Name        @"zh"
#define kEN_Name        @"en"
#define kKO_Name        @"ko"
#define kRU_Name        @"ru"
#define kJA_Name        @"ja"

#import <Foundation/Foundation.h>


/**
 解码的回调
 @param repeat 有重复值的键
 @param oldKeys 原文件的key的顺序
 @param notesDic 原文件中的所有注释（包括换行）
 */
typedef void(^DecodeBlock)(NSArray *repeat,NSArray *oldKeys,NSDictionary *notesDic);

/**
 编码的回调
 @param nofundKeys 未找到（未翻译）的key
 */
typedef void(^EncodeBlock)(NSArray *nofundKeys);

@interface GZCLanguageCheckTool : NSObject


/// 根据宏定义自动检测并输出对应文件
+ (void)automaticCheck;

/**
 解码对应文件
 
 @param filePath 文件路径（全）
 @param block 回调方法(可以传nil)
 @return 所有键值对的字典
 */
+ (NSDictionary *)decodeFile:(NSString *)filePath
                       block:(DecodeBlock)block;

/**
 编码对应的数据并写入文件
 
 @param filePath 写入文件路径
 @param keys 所有需要编码的key
 @param dic 所有键值对的字典
 @param notes 所有注释的字典
 @param block 回调方法（可以为nil）
 @return 是否成功
 */
+ (BOOL)encodeToFile:(NSString *)filePath
                keys:(NSArray <NSString *>*)keys
                 dic:(NSDictionary *)dic
               notes:(NSDictionary *)notes
               block:(EncodeBlock)block;

/**
 编码对应的数据
 
 @param keys 所有需要编码的key
 @param dic 所有键值对的字典
 @param zhDic 中文对照键值对的字典
 @param notes 所有注释的字典
 @param block 回调方法（可以为nil）
 @return 编码后的字符串(可以直接写入文件)
 */
+ (NSString *)encodeByKeys:(NSArray <NSString *>*)keys
                       dic:(NSDictionary *)dic
                     zhDic:(NSDictionary *)zhDic
                     notes:(NSDictionary *)notes
                     block:(EncodeBlock)block;

/**
 对keys排序
 @param keys 需要排序的keys
 @return 排序后的Array
 */
+ (NSArray *)sortKeys:(NSArray *)keys;

/**
 写入到文件中
 
 @param str 字符串
 @param filePath 文件路径
 @return 是否成功
 */
+ (BOOL)writeString:(NSString *)str
             toPath:(NSString *)filePath;

+ (NSString *)pathWithString:(NSString *)name;

+ (NSString *)writePathWithString:(NSString *)name;

@end
