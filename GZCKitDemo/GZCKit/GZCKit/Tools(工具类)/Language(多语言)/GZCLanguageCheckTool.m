//
//  GZCLanguageCheckTool.m
//  GZCKit
//
//  Created by GuoZhongCheng on 2018/7/30.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//

#import "GZCLanguageCheckTool.h"

@implementation GZCLanguageCheckTool
+ (void)automaticCheck{
    NSMutableArray *zhKeys = [NSMutableArray array];
    NSMutableDictionary *zhNotesDic = [NSMutableDictionary dictionary];
    
    NSDictionary *zhDic = [self decodeFile:[self pathWithString:kZH_Name] block:^(NSArray *repeat, NSArray *oldKeys, NSDictionary *notesDic) {
        [zhKeys addObjectsFromArray:oldKeys];
        [zhNotesDic addEntriesFromDictionary:notesDic];
        NSLog(@"中文重复%ld条:%@",repeat.count,repeat);
    }];
    NSDictionary *enDic = [self decodeFile:[self pathWithString:kEN_Name] block:^(NSArray *repeat, NSArray *oldKeys, NSDictionary *notesDic) {
        NSLog(@"英文重复%ld条:%@",repeat.count,repeat);
    }];
    NSDictionary *jaDic = [self decodeFile:[self pathWithString:kJA_Name] block:^(NSArray *repeat, NSArray *oldKeys, NSDictionary *notesDic) {
        NSLog(@"日语重复%ld条:%@",repeat.count,repeat);
    }];
    NSDictionary *koDic = [self decodeFile:[self pathWithString:kKO_Name] block:^(NSArray *repeat, NSArray *oldKeys, NSDictionary *notesDic) {
        NSLog(@"韩语重复%ld条:%@",repeat.count,repeat);
    }];
    NSDictionary *ruDic = [self decodeFile:[self pathWithString:kRU_Name] block:^(NSArray *repeat, NSArray *oldKeys, NSDictionary *notesDic) {
        NSLog(@"俄语重复%ld条:%@",repeat.count,repeat);
    }];
    
    //    NSArray *zhKeys = [self keysWithDict:zhDic];
    [self encodeToFile:[self writePathWithString:kZH_Name] keys:zhKeys dic:zhDic notes:zhNotesDic block:nil];
    
    NSString *enChangeStr = [self encodeByKeys:zhKeys dic:enDic zhDic:zhDic notes:zhNotesDic block:^(NSArray *nofundKeys) {
        if (nofundKeys.count == 0) {
            return;
        }
        NSLog(@"英语未翻译字段:");
        [nofundKeys enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSLog(@"\"%@\" = \"%@\"",obj,zhDic[obj]);
        }];
        NSLog(@"\n");
        [self encodeToFile:[self writePathWithString:@"英语未翻译字段"] keys:nofundKeys dic:zhDic notes:nil block:nil];
    }];
    [self writeString:enChangeStr toPath:[self writePathWithString:kEN_Name]];
    
    NSString *jaChangeStr = [self encodeByKeys:zhKeys dic:jaDic zhDic:zhDic notes:zhNotesDic block:^(NSArray *nofundKeys) {
        if (nofundKeys.count == 0) {
            return;
        }
        NSLog(@"日语未翻译字段:");
        [nofundKeys enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSLog(@"\"%@\" = \"%@\"",obj,zhDic[obj]);
        }];
        NSLog(@"\n");
        [self encodeToFile:[self writePathWithString:@"日语未翻译字段"] keys:nofundKeys dic:zhDic notes:nil block:nil];
    }];
    [self writeString:jaChangeStr toPath:[self writePathWithString:kJA_Name]];
    
    NSString *koChangeStr = [self encodeByKeys:zhKeys dic:koDic zhDic:zhDic notes:zhNotesDic block:^(NSArray *nofundKeys) {
        if (nofundKeys.count == 0) {
            return;
        }
        NSLog(@"韩语未翻译字段:");
        [nofundKeys enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSLog(@"\"%@\" = \"%@\"",obj,zhDic[obj]);
        }];
        NSLog(@"\n");
        [self encodeToFile:[self writePathWithString:@"韩语未翻译字段"] keys:nofundKeys dic:zhDic notes:nil block:nil];
    }];
    [self writeString:koChangeStr toPath:[self writePathWithString:kKO_Name]];
    
    NSString *ruChangeStr = [self encodeByKeys:zhKeys dic:ruDic zhDic:zhDic notes:zhNotesDic block:^(NSArray *nofundKeys) {
        if (nofundKeys.count == 0) {
            return;
        }
        NSLog(@"俄语未翻译字段:");
        [nofundKeys enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSLog(@"\"%@\" = \"%@\"",obj,zhDic[obj]);
        }];
        NSLog(@"\n");
        [self encodeToFile:[self writePathWithString:@"俄语未翻译字段"] keys:nofundKeys dic:zhDic notes:nil block:nil];
    }];
    [self writeString:ruChangeStr toPath:[self writePathWithString:kRU_Name]];
}

+ (NSDictionary *)decodeFile:(NSString *)filePath block:(DecodeBlock)block{
    BOOL next = YES;
    NSString *tempStr = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];;
    NSMutableDictionary *mutDic = [NSMutableDictionary dictionary];
    NSMutableArray *repeat = [NSMutableArray array];
    NSMutableArray *oldKeys = [NSMutableArray array];
    NSMutableDictionary *notesDic = [NSMutableDictionary dictionary];
    while (next) {
        NSMutableString *notesString = [NSMutableString string];
        BOOL nextNotes = YES;
        while (nextNotes) {
            BOOL nextEnter = YES;
            while (nextEnter) {
                nextEnter = NO;
                NSRange enter = [tempStr rangeOfString:@"\n"];
                if (enter.location == 0) {
                    [notesString appendString:[tempStr substringToIndex:enter.length]];
                    tempStr = [tempStr substringFromIndex:enter.length];
                    nextEnter = YES;
                }
            }
            nextNotes = NO;
            NSRange notes = [tempStr rangeOfString:@"/*"];
            if (notes.location == 0) {
                NSRange endRange = [tempStr rangeOfString:@"*/\n"];
                [notesString appendString:[tempStr substringToIndex:endRange.location + endRange.length]];
                tempStr = [tempStr substringFromIndex:endRange.location + endRange.length];
                nextNotes = YES;
            }
            NSRange ignor = [tempStr rangeOfString:@"//"];
            if (ignor.location == 0) {
                NSRange endRange = [tempStr rangeOfString:@"\n"];
                [notesString appendString:[tempStr substringToIndex:endRange.location + endRange.length]];
                tempStr = [tempStr substringFromIndex:endRange.location + endRange.length];
                nextNotes = YES;
            }
        }
        NSRange range = [tempStr rangeOfString:@"\""];
        if (range.length) {
            NSString *subStr = [tempStr substringFromIndex:(range.location + range.length)];
            NSRange endRange = [subStr rangeOfString:@"\""];
            if (endRange.location > subStr.length) {
                NSLog(@"%ld",mutDic.count);
            }
            NSString *key = [subStr substringWithRange:NSMakeRange(0, endRange.location)];
            subStr = [subStr substringFromIndex:endRange.location + endRange.length];
            
            NSRange valueBeginRange = [subStr rangeOfString:@"\""];
            subStr = [subStr substringFromIndex:(valueBeginRange.location + valueBeginRange.length)];
            NSRange lineRange = [subStr rangeOfString:@"\n"];
            endRange = [subStr rangeOfString:@"\"" options:NSBackwardsSearch range:NSMakeRange(0, lineRange.location)];
            if (endRange.location > subStr.length) {
                NSLog(@"%ld",mutDic.count);
            }
            NSString *value = [subStr substringWithRange:NSMakeRange(0, endRange.location)];
            if ([mutDic objectForKey:key]) {
                [repeat addObject:key];
                [oldKeys removeObject:key];
                [oldKeys addObject:key];
                if (notesDic[key]!=nil) {
                    [notesDic removeObjectForKey:key];
                }
            }else{
                [oldKeys addObject:key];
            }
            if (notesString.length > 0) {
                [notesDic setObject:notesString forKey:key];
            }
//            NSLog(@"%@:%@\n",key,value);
            [mutDic setValue:value forKey:key];
            subStr = [subStr substringFromIndex:lineRange.location + lineRange.length];
            tempStr = subStr;
            next = YES;
        }else{
            next = NO;
        }
    }
    if (block) {
        block(repeat,oldKeys,notesDic);
    }
    return mutDic;
}

+ (BOOL)encodeToFile:(NSString *)filePath keys:(NSArray<NSString *> *)keys dic:(NSDictionary *)dic notes:(NSDictionary *)notes block:(EncodeBlock)block{
    return [self writeString:[self encodeByKeys:keys dic:dic zhDic:nil notes:notes block:block] toPath:filePath];
}

+ (NSString *)encodeByKeys:(NSArray <NSString *>*)keys
                       dic:(NSDictionary *)dic
                     zhDic:(NSDictionary *)zhDic
                     notes:(NSDictionary *)notes
                     block:(EncodeBlock)block{
    NSMutableString *str = [NSMutableString string];
    NSMutableArray *nofunds = [NSMutableArray array];
    __block NSInteger keyLength = 0;
    [keys enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        keyLength = MAX(obj.length, keyLength);
    }];
    // 太长了就只取50,以免大面积出现一行放不下
    keyLength = MIN(50, keyLength);
    [keys enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        __block NSString *value = dic[obj];
        if (value == nil || [value isEqualToString:@"未翻译"]) {
            if (zhDic) {
                NSString *zhObj = zhDic[obj];
                [zhDic enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull zhkey, id  _Nonnull zhobj, BOOL * _Nonnull stop) {
                    if ([zhObj isEqualToString:zhobj]) {
                        if (![zhkey isEqualToString:obj]) {
                            value = dic[zhkey];
                            if (value.length > 0) {
                                *stop = YES;
                            }
                        }
                    }
                }];
                if (value.length == 0 || [value isEqualToString:@"未翻译"]) {
                    value = @"未翻译";
                    [nofunds addObject:obj];
                }
            }else{
                value = @"未翻译";
                [nofunds addObject:obj];
            }
        }
        if (notes[obj] != nil) {
            [str appendString:notes[obj]];
        }
        [str appendFormat:@"\"%@\"",obj];
        if (keyLength > obj.length) {
            for (int i = 0; i < keyLength - obj.length; i ++) {
                [str appendString:@" "];
            }
        }
        [str appendFormat:@"=   \"%@\";\n",value];
    }];
    if (block) {
        block(nofunds);
    }
    return str;
}

+ (NSArray *)sortKeys:(NSArray *)keys{
    NSArray *sortedArray = [keys sortedArrayUsingComparator:^NSComparisonResult(id obj1,id obj2) {
        return[obj1 compare:obj2 options:NSNumericSearch];//正序
    }];
    return sortedArray;
}

+ (BOOL)writeString:(NSString *)str toPath:(NSString *)filePath{
    NSError *error;
    [str writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&error];
    return !(error == nil);
}

+ (NSString *)pathWithString:(NSString *)name{
    return [NSString stringWithFormat:@"%@%@.txt",kBasePath,name];
}

+ (NSString *)writePathWithString:(NSString *)name{
    return [NSString stringWithFormat:@"%@%@.txt",kBaseOutPath,name];
}

@end

