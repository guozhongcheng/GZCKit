//
//  GZCFPSLabel.h
//  GZCKit
//
//  Created by GuoZhongCheng on 2018/6/29.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//
//  fps显示工具，直接添加到window上即可

#import <UIKit/UIKit.h>


/**
 显示FPS(仿YYFPSLabel)
 最大值：
 OSX/iOS    60.00
 iPhone     59.97
 iPad       60.0
 */
@interface GZCFPSLabel : UILabel

@end
