//
//  NSString+RSA.h
//  GZCKit
//
//  Created by ZhongCheng Guo on 2018/4/11.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RSAKeyHandler : NSObject

/// 私钥
@property (nonatomic,assign) SecKeyRef privateKey;

/// 公钥
@property (nonatomic,assign) SecKeyRef publicKey;

+ (instancetype)shareInstance;

/// 使用缓存将私钥和公钥缓存起来，以减少重复获取带来的性能损失
/**
 获取私钥
 
 @param privateKeyFileName 文件名
 @param password 私钥密码
 @return 私钥信息
 */
+(SecKeyRef)getPrivateKeyRef:(NSString *)privateKeyFileName
                    passWord:(NSString *)password;

/**
 获取公钥
 
 @param publicKeyFileName 文件名
 @return 私钥信息
 */
+(SecKeyRef)getPublicKeyRef:(NSString *)publicKeyFileName;

@end

@interface NSString (RSA)

/**
 * 加密
 * originalString 原始字符串
 */
+(NSString *)encryptRSA:(NSString *) originalString
              publicKey:(SecKeyRef)publicKey;

/**
 * 解密
 *
 * ciphertextString 加密字符串
 */
+(NSString *)decryptRSA:(NSString *) ciphertextString
             privateKey:(SecKeyRef)privateKey;



@end
