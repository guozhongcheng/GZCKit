//
//  NSString+MD5.h
//  GZCKit
//
//  Created by ZhongCheng Guo on 2018/3/31.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MD5)

/**
 字符串md5加密
 
 @return 加密后的字符串
 */
- (NSString *)md5String;


/**
 data进行md5加密

 @param data data数据
 @return 加密后的字符串
 */
+ (NSString *)md5Data:(NSData *)data;

@end
