//
//  NSString+SHA_1.h
//  GZCKit
//
//  Created by ZhongCheng Guo on 2018/3/31.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (SHA_1)

/**
 Sha1加密
 
 @return 加密后的字符串
 */
- (NSString *)encodeSHA1;

@end
