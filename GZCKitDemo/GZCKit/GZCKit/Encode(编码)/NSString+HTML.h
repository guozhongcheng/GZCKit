//
//  NSString+HTML.h
//  GZCKit
//
//  Created by ZhongCheng Guo on 2018/3/31.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (HTML)

/// 将 &lt 等类似的字符转化为HTML中的“<”等
- (NSString *)htmlEntityDecode;

/// 为 html 字符串中的img地址添加host
- (NSString *)appendingImageHost:(NSString *)host;

/// 调整图片大小及p的间距
- (NSString *)htmlImgAdjustmentWidth:(float)width;

/// 将HTML字符串转化为NSAttributedString富文本字符串
- (NSAttributedString *)attributedStringWithHTMLString;

//去掉 HTML 字符串中的标签
- (NSString *)filterHTML;

@end
