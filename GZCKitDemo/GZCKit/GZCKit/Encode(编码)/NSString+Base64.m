//
//  NSString+Base64.m
//  GZCKit
//
//  Created by ZhongCheng Guo on 2018/3/31.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//

#import "NSString+Base64.h"

@implementation NSString (Base64)

+(NSString *)encodeBase64:(NSData *)data{
    NSString* encodeResult = [data base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
    return encodeResult;
}

-(NSString *)encodeBase64String{
    NSData* originData = [self dataUsingEncoding:NSASCIIStringEncoding];
    NSString* encodeResult = [originData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
    return encodeResult;
}

+(NSData *)decodeBase64:(NSString *)string{
    NSData* decodeData = [[NSData alloc] initWithBase64EncodedString:string options:0];
    return decodeData;
}

-(NSString *)decodeBase64String{
    NSData* decodeData = [[NSData alloc] initWithBase64EncodedString:self options:0];
    NSString* decodeStr = [[NSString alloc] initWithData:decodeData encoding:NSASCIIStringEncoding];
    return decodeStr;
}

@end
