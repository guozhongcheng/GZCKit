//
//  NSString+SHA_1.m
//  GZCKit
//
//  Created by ZhongCheng Guo on 2018/3/31.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//

#import "NSString+SHA_1.h"
#import <CommonCrypto/CommonDigest.h>

@implementation NSString (SHA_1)

- (NSString *)encodeSHA1{
    const char *cstr = [self cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:self.length];
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    CC_SHA1(data.bytes, (unsigned int)data.length, digest);
    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++) {
        [output appendFormat:@"%02x", digest[i]];
    }
    return output;
}

@end
