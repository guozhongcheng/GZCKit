//
//  NSString+Base64.h
//  GZCKit
//
//  Created by ZhongCheng Guo on 2018/3/31.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NSString*(^EncodedString)(NSString *string);

@interface NSString (Base64)

/**
 对数据进行Base64加密
 
 @param data 需要加密的数据
 @return 加密后的字符串
 */
+ (NSString *)encodeBase64:(NSData *)data;

/**
 对字符串进行Base64加密
 
 @return 加密后的字符串
 */
- (NSString *)encodeBase64String;

/**
 对字符串进行Base64解密
 
 @param string 加密后的字符串
 @return 解密后的字符串
 */
+ (NSData *)decodeBase64:(NSString *)string;

/**
 对字符串进行Base64解密
 
 @return 解密后的字符串
 */
- (NSString *)decodeBase64String;

@end
