//
//  NSString+URL.m
//  GZCKit
//
//  Created by ZhongCheng Guo on 2018/3/31.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//

#import "NSString+URL.h"
#import <UIKit/UIKit.h>

@implementation NSString (URL)

- (NSString*)urlEncode
{
    return [self stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet characterSetWithCharactersInString:@"!*'();:@&=+$,/?%#[]"]];
}

- (NSString*)urlDecode
{
    return [self stringByRemovingPercentEncoding];
}

@end
