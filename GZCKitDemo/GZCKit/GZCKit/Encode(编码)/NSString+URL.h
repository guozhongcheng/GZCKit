//
//  NSString+URL.h
//  GZCKit
//
//  Created by ZhongCheng Guo on 2018/3/31.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (URL)

/**
 UrlEncode编码
 对URL中的中文进行编码
 @return 编码后的字符串
 */
- (NSString*)urlEncode;

/**
 UrlDecode解码
 
 @return 解码后的字符串
 */
- (NSString*)urlDecode;

@end
