//
//  NSString+AES.h
//  GZCKit
//
//  Created by ZhongCheng Guo on 2018/3/31.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (AES)

/**
 AES256加密
 
 @param key 密钥
 @param vi 盐
 @return 加密后的字符串
 */
- (NSData *)encryptAES256WithKey:(NSString *)key
                              vi:(NSString *)vi;
/**
 AES256解密
 
 @param key 密钥
 @param vi 盐
 @return 加密后的字符串
 */
- (NSData *)decryptAES256WithKey:(NSString *)key
                              vi:(NSString *)vi;

@end
