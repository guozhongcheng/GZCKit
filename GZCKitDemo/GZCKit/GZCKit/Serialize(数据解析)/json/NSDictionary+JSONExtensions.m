//
//  NSDictionary+JSONExtensions.m
//  GZCKit
//
//  Created by ZhongCheng Guo on 2018/4/1.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//

#import "NSDictionary+JSONExtensions.h"

@implementation NSDictionary (JSONExtensions)

+ (nullable id)dictionaryWithJSONData:(NSData *)inData error:(NSError **)outError
{
    return([NSJSONSerialization JSONObjectWithData:inData options:NSJSONReadingAllowFragments error:outError]);
}

+ (nullable id)dictionaryWithJSONString:(NSString *)inJSON error:(NSError **)outError;
{
    NSData *theData = [inJSON dataUsingEncoding:NSUTF8StringEncoding];
    return([self dictionaryWithJSONData:theData error:outError]);
}

@end
