//
//  NSDictionary+JSONExtensions.h
//  GZCKit
//
//  Created by ZhongCheng Guo on 2018/4/1.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (JSONExtensions)

/**
 解析JSON数据

 @param inData 需要解析的JSON数据
 @param outError 错误信息
 @return 解析后的结果
 */
+ (id)dictionaryWithJSONData:(NSData *)inData
                       error:(NSError **)outError;

/**
 解析JSON字符串

 @param inJSON 需要解析的JSON字符串
 @param outError 错误信息
 @return 解析后的结果
 */
+ (id)dictionaryWithJSONString:(NSString *)inJSON
                         error:(NSError **)outError;

@end
