//
//  GZCGridAdapter.h
//  GZCKit
//
//  Created by GuoZhongCheng on 2018/6/29.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GZCGridCell;
@protocol GZCGridAdapterProtocol

@required
/**
 cell中的点击事件

 @param cell 事件触发cell
 @param type 事件的type
 @param indexPath cell的indexPath
 */
- (void)cellTarget:(GZCGridCell *)cell
        targetType:(NSInteger)type
           atIndex:(NSIndexPath *)indexPath;


/**
 根据model对象计算size并返回

 @param model model对象
 */
- (void)sizeWithModel:(id)model;

@end

@interface GZCGridAdapter : NSObject <GZCGridAdapterProtocol>

/// 代理
@property ( nonatomic, weak ) id delegate;

- (instancetype)initWithModelNamed:(NSString *)modelName
                         cellNamed:(NSString *)cellName;
+ (instancetype)adapterWithModelNamed:(NSString *)modelName
                            cellNamed:(NSString *)cellName;

- (instancetype)cellAtIndexPath:(NSIndexPath *)indexPath
                          model:(id)model;

- (void)clearData;

@end
