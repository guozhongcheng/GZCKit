//
//  GZCGridAdapter.m
//  GZCKit
//
//  Created by GuoZhongCheng on 2018/6/29.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//

#import "GZCGridAdapter.h"
#import "GZCGridCell.h"

@interface GZCGridAdapter()

/// 存放Cell的数组
@property ( nonatomic, strong ) NSMutableDictionary <NSIndexPath *,GZCGridCell *> *dataCells;

/// 存放IndexPath对应的数据
@property ( nonatomic, strong ) NSMutableDictionary <NSIndexPath *,id> *dataModels;

/// model类型名称
@property ( atomic ) Class modelClass;

/// cell类型名称
@property ( atomic ) Class cellClass;

@end

@implementation GZCGridAdapter

- (instancetype)initWithModelNamed:(NSString *)modelName
                        cellNamed:(NSString *)cellName{
    self = [super init];
    if (self) {
        self.modelClass = NSClassFromString(modelName);
        self.cellClass = NSClassFromString(cellName);
    }
    return self;
}

+ (instancetype)adapterWithModelNamed:(NSString *)modelName cellNamed:(NSString *)cellName{
    return [[self alloc]initWithModelNamed:modelName cellNamed:cellName];
}

- (GZCGridCell *)getCellAtIndexPath:(NSIndexPath *)indexPath
                    model:(id)model{
    GZCGridCell *cell = self.dataCells[indexPath];
    if (cell == nil) {
        cell = [[self.cellClass alloc]init];
    }
    self.dataModels[indexPath] = model;
    return cell;
}

- (void)clearData{
    [self.dataModels removeAllObjects];
    [self.dataCells removeAllObjects];
}

- (NSMutableDictionary<NSIndexPath *,GZCGridCell *> *)dataCells{
    if (_dataCells == nil) {
        _dataCells = [NSMutableDictionary dictionary];
    }
    return _dataCells;
}

- (NSMutableDictionary<NSIndexPath *,id> *)dataModels{
    if (_dataModels == nil) {
        _dataModels = [NSMutableDictionary dictionary];
    }
    return _dataModels;
}


- (NSString *)keyFromIndexPath:(NSIndexPath *)indexPath{
    return [NSString stringWithFormat:@"row%ld section%ld",indexPath.row,indexPath.section];
}


#pragma mark protocol Methods
-(void)bindModel:(id)model toCell:(GZCGridCell *)cell{
    
}

-(void)cellTarget:(GZCGridCell *)cell targetType:(NSInteger)type atIndex:(NSIndexPath *)indexPath{

}

@end
