//
//  GZCGridCell.h
//  GZCKit
//
//  Created by GuoZhongCheng on 2018/6/29.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//

#import <AsyncDisplayKit/ASCellNode.h>

@interface GZCGridCell : ASCellNode

/**
 定义回调事件的Block

 @param targetType 回调事件的type，可以与空间的tag对应，也可以自己定义
 @param cell 回调的事件的触发Cell
 */
typedef void(^GZCGridCellTarget)(NSInteger targetType,GZCGridCell *cell);


@end
