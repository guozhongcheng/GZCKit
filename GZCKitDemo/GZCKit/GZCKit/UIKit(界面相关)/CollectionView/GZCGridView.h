//
//  GZCGridView.h
//  GZCKit
//
//  Created by ZhongCheng Guo on 2018/6/26.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//
//  对CollectionView的封装，简化使用方式

#import <UIKit/UIKit.h>
#import <AsyncDisplayKit/AsyncDisplayKit.h>
#import "GZCGridCell.h"
#import "GZCGridAdapter.h"

@class GZCGridView;
@protocol GZCGridViewDelegate

@optional

/**
 GZCGridView点击事件回调

 @param gridView GZCGridView对象
 @param cell GZCGridCell类型的Cell
 @param event GZCGridCell的event事件（多个按钮点击时用于区分不同事件）
 */
- (void)gridView:(GZCGridView *)gridView
 didSelectedCell:(GZCGridCell *)cell
           event:(id)event;

@end

@interface GZCGridView : NSObject

/// 代理
@property ( nonatomic, weak ) id<GZCGridViewDelegate> delegate;

/// 内容的边距
@property ( nonatomic, assign ) UIEdgeInsets contentEdgeInsets;

/// 列间距
@property ( nonatomic, assign ) CGFloat columnSpacing;

/// 行间距
@property ( nonatomic, assign ) CGFloat lineSpacing;

/// 默认行高
@property ( nonatomic, assign ) CGFloat defaultCellHeight;

/**
 滚动方向,值为：
 ASScrollDirectionNone  = 0,
 ASScrollDirectionRight = 1 << 0,
 ASScrollDirectionLeft  = 1 << 1,
 ASScrollDirectionUp    = 1 << 2,
 ASScrollDirectionDown  = 1 << 3
 */
@property ( nonatomic, assign ) ASScrollDirection scrollDirection;

/// 注册适配器
@property ( nonatomic, strong ) NSArray <GZCGridAdapter *>*registeredAdapters;

/// 数据源
@property ( nonatomic, strong ) NSMutableArray *datas;

/// 显示内容的主控件，这里是collectionNode
@property ( nonatomic, strong, readonly ) ASCollectionNode *collectionNode;

/**
 初始化方法
 
 @param frame frame
 @return GZCGridView对象
 */
- (instancetype)initWithFrame:(CGRect)frame;
+ (instancetype)gridViewWithFrame:(CGRect)frame;

/**
 注册适配器

 @param adapter 适配器对象
 */
+ (void)registerAdapter:(GZCGridAdapter *)adapter;

@end
