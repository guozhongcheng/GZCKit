//
//  GZCSegmentedBoxControl.m
//  GZCKitDemo
//
//  Created by GuoZhongCheng on 2018/7/28.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//

#import "GZCSegmentedBoxControl.h"

@interface GZCSegmentedBoxControl ()

@property (nonatomic, strong) CALayer *selectionIndicatorBoxLayer;
@property (nonatomic, strong) CALayer *selectionIndicatorBoxMashLayer;

@end

@implementation GZCSegmentedBoxControl

- (void)setSelectedBoxBackgroundColors:(NSArray<GZCGradientColor *> *)selectedBoxBackgroundColors{
    NSAssert((selectedBoxBackgroundColors.count == 1) || (selectedBoxBackgroundColors.count == self.sectionTitles.count), @"selectedBoxBackgroundColors个数不合适，必须等于1或等于title个数");
    [super setSelectedBoxBackgroundColors:selectedBoxBackgroundColors];
    if (selectedBoxBackgroundColors.count == 1) {
        return;
    }
}

- (void)setSelectedBoxTitleColors:(NSArray<UIColor *> *)selectedBoxTitleColors{
    NSAssert(selectedBoxTitleColors == nil || selectedBoxTitleColors.count == self.sectionTitles.count, @"selectedBoxTitleColors个数不合适，必须等于1或等于于title个数");
    [super setSelectedBoxTitleColors:selectedBoxTitleColors];
}

- (void)drawRect:(CGRect)rect{
    [super drawRect:rect];
    
    [self.segmentEnableWidthsArray enumerateObjectsUsingBlock:^(NSNumber * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        CGRect frame = [self frameForBoderAtIndex:idx];
        if (idx == self.selectedSegmentIndex && self.selectedBoxBackgroundColors.count > 0) {
            CAGradientLayer *backgroundLayer;
            if (self.selectedBoxBackgroundColors.count == 1) {
                backgroundLayer = [self.selectedBoxBackgroundColors[0] transformationToLayer];
            }else{
                backgroundLayer = [self.selectedBoxBackgroundColors[idx] transformationToLayer];
            }
            CAShapeLayer *pathLayer = [self createShapeLayerWithPath:[self pathForBorderAtIndex:idx frame:frame]];
            backgroundLayer.frame = frame;
            backgroundLayer.mask = pathLayer;
            [self.scrollView.layer insertSublayer:backgroundLayer atIndex:0];
            //            [self.scrollView.layer addSublayer:backgroundLayer];
        }else{
            CAShapeLayer *pathLayer = [self createShapeLayerWithPath:[self pathForBorderAtIndex:idx frame:frame]];
            pathLayer.frame = frame;
            [self.scrollView.layer insertSublayer:pathLayer atIndex:0];
        }
    }];
}

- (CGRect)frameForBoderAtIndex:(NSInteger)index{
    __block CGFloat indicatorXOffset = 0.0f;
    CGFloat indicatorYOffset = 0.0f;
    __block CGFloat indicatorWidth = 0.0f;
    CGFloat indicatorHeight = self.frame.size.height;
    [self.segmentEnableWidthsArray enumerateObjectsUsingBlock:^(NSNumber * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (idx < index) {
            indicatorXOffset += obj.floatValue;
        }
        if (idx == index) {
            indicatorWidth = obj.floatValue;
        }
    }];
    if (index == 0) {
        indicatorWidth -= self.segmentSpacing/2;
    }else if (index == self.segmentEnableWidthsArray.count - 1){
        indicatorXOffset += self.segmentSpacing/2;
        indicatorWidth -= self.segmentSpacing/2;
    }else{
        indicatorXOffset += self.segmentSpacing/2;
        indicatorWidth -= self.segmentSpacing;
    }
    return CGRectMake(indicatorXOffset, indicatorYOffset, indicatorWidth, indicatorHeight);
}

- (UIBezierPath *)pathForBorderAtIndex:(NSInteger)index
                                 frame:(CGRect)frame {
    CGFloat spacing = self.borderWidth / 2;
    CGFloat indicatorXOffset = spacing;
    CGFloat indicatorYOffset = spacing;
    CGFloat indicatorWidth = frame.size.width - spacing * 2;
    CGFloat indicatorHeight = frame.size.height - spacing * 2;
    
    CGPoint pointLT = CGPointMake(indicatorXOffset, indicatorYOffset);
    CGPoint pointRT = CGPointMake(indicatorXOffset + indicatorWidth, indicatorYOffset);
    CGPoint pointRB = CGPointMake(indicatorXOffset + indicatorWidth, indicatorYOffset + indicatorHeight);
    CGPoint pointLB = CGPointMake(indicatorXOffset, indicatorYOffset + indicatorHeight);
    UIBezierPath *path = [UIBezierPath bezierPath];
    if (self.contentRadiusEdgeInsets.top > 0 && index == 0) {
        [path moveToPoint:CGPointMake(indicatorXOffset, indicatorYOffset + self.contentRadiusEdgeInsets.top)];
        [path addQuadCurveToPoint:CGPointMake(indicatorXOffset + self.contentRadiusEdgeInsets.top, indicatorYOffset) controlPoint:pointLT];
    }else{
        [path moveToPoint:pointLT];
    }
    if (self.contentRadiusEdgeInsets.right > 0 && index == self.segmentEnableWidthsArray.count - 1) {
        [path addLineToPoint:CGPointMake(indicatorXOffset + indicatorWidth - self.contentRadiusEdgeInsets.right, indicatorYOffset)];
        [path addQuadCurveToPoint:CGPointMake(indicatorXOffset + indicatorWidth, indicatorYOffset + self.contentRadiusEdgeInsets.right) controlPoint:pointRT];
    }else{
        [path addLineToPoint:pointRT];
    }
    if (self.contentRadiusEdgeInsets.bottom > 0 && index == self.segmentEnableWidthsArray.count - 1) {
        [path addLineToPoint:CGPointMake(indicatorXOffset + indicatorWidth, indicatorYOffset + indicatorHeight - self.contentRadiusEdgeInsets.bottom)];
        [path addQuadCurveToPoint:CGPointMake(indicatorXOffset + indicatorWidth - self.contentRadiusEdgeInsets.bottom, indicatorYOffset + indicatorHeight) controlPoint:pointRB];
    }else{
        [path addLineToPoint:pointRB];
    }
    if (self.contentRadiusEdgeInsets.left > 0 && index == 0) {
        [path addLineToPoint:CGPointMake(indicatorXOffset + self.contentRadiusEdgeInsets.left, indicatorYOffset + indicatorHeight)];
        [path addQuadCurveToPoint:CGPointMake(indicatorXOffset, indicatorYOffset + indicatorHeight - self.contentRadiusEdgeInsets.left) controlPoint:pointLB];
    }else{
        [path addLineToPoint:pointLB];
    }
    [path closePath];
    return path;
}

-(CAShapeLayer *)createShapeLayerWithPath:(UIBezierPath *)path {
    CAShapeLayer * layer = [CAShapeLayer layer];
    layer.path           = path.CGPath;
    layer.strokeColor    = self.borderColor.CGColor;
    if (self.selectionStyle == GZCSegmentedControlSelectionStyleBoxFill) {
        layer.fillColor      = self.backgroundColor.CGColor;
    }else{
        layer.fillColor      = [UIColor clearColor].CGColor;
    }
    layer.lineWidth      = self.borderWidth;
    return layer;
}

- (NSDictionary *)resultingSelectedTitleTextAttributes {
    
    NSMutableDictionary *resultingAttrs = [NSMutableDictionary dictionaryWithDictionary:[self resultingTitleTextAttributes]];
    if (self.selectedTitleTextAttributes) {
        [resultingAttrs addEntriesFromDictionary:self.selectedTitleTextAttributes];
    }
    
    if (self.selectedBoxTitleColors.count) {
        UIColor *color = self.selectedBoxTitleColors[self.selectedSegmentIndex];
        [resultingAttrs setObject:color forKey:NSForegroundColorAttributeName];
    }
    return [resultingAttrs copy];
}


#pragma mark - 懒加载
- (CALayer *)selectionIndicatorBoxLayer{
    if (!_selectionIndicatorBoxLayer) {
        _selectionIndicatorBoxLayer = [CALayer layer];
    }
    return _selectionIndicatorBoxLayer;
}
@end
