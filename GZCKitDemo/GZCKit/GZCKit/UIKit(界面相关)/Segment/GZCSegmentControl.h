//
//  GZCSegmentControl.h
//  GZCKit
//
//  Created by GuoZhongCheng on 2018/7/28.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GZCBlockConstants.h"

/// 分段选择器样式
//typedef NS_ENUM(NSUInteger, GZCSegmentedControlType) {
//    GZCSegmentedControlTypeText,         /// 纯文字
//};

/// 选中样式
typedef NS_ENUM(NSUInteger, GZCSegmentedControlSelectionStyle) {
    /**
     选中时只改变文字颜色
     **/
    GZCSegmentedControlSelectionStyleNone,
    
    /**
     滑块，宽度与文本内容宽度一致:
     texttext       texttexttexttext
     ━━━━━━━━       ━━━━━━━━━━━━━━━━
     **/
    GZCSegmentedControlSelectionStyleTextWidthStripe,
    
    /**
     滑块，固定宽度
     texttext    texttexttext
     ━━            ━━
     **/
    GZCSegmentedControlSelectionStyleFixedWidthStripe,
    
    /**
     滑块，宽度为segment可点击宽度
     texttext    texttexttext
     ━━━━━━━━━━
     **/
    GZCSegmentedControlSelectionStyleEnableWidthStripe,
    
    /**
     选中分段改变文字和边框颜色
     ┏━━━━━━━━━━━━┓┌┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┐
     ┃    text    ┃|    texttext    |
     ┗━━━━━━━━━━━━┛└┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┘
     **/
    GZCSegmentedControlSelectionStyleBox,
    
    /**
     选中分段背景填充
     ┏━━━━━━━━━━━━┓┌┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┐
     ┃////text////┃|    texttext    |
     ┗━━━━━━━━━━━━┛└┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┘
     **/
    GZCSegmentedControlSelectionStyleBoxFill,
};

/// 每段宽度的样式
typedef NS_ENUM(NSInteger, GZCSegmentedControlSegmentWidthStyle) {
    /// 每段的宽度相同
    GZCSegmentedControlSegmentWidthStyleFixed,
    /// 根据文字计算宽度
    GZCSegmentedControlSegmentWidthStyleDynamic,
    /// 均分宽度，根据title个数均分宽度，如果有某段文字超过这个宽度则会自动取改为根据文字计算宽度
    GZCSegmentedControlSegmentWidthStyleEquable,
};

/// 对齐方式,仅当内容宽度小于整体宽度时适用
typedef NS_ENUM(NSUInteger, GZCSegmentedControlSegmentAlignment) {
    /**
     居中:
     | -多余宽度- TEXT -segmentSpacing- TEXT -segmentSpacing- TEXT -多余宽度- |
     **/
    GZCSegmentedControlSegmentAlignmentCenter,
    
    /**
     居左:
     | TEXT -segmentSpacing- TEXT -segmentSpacing- TEXT --多余宽度-- |
     **/
    GZCSegmentedControlSegmentAlignmentLeft,
    
    /**
     居右:
     | --多余宽度-- TEXT -segmentSpacing- TEXT -segmentSpacing- TEXT |
     **/
    GZCSegmentedControlSegmentAlignmentRight,
    
    /**
     两侧分别居左和居右，中间的居中，一般仅适用于三个title的情况:
     | TEXT --多余宽度-- TEXT --多余宽度-- TEXT |
     **/
    GZCSegmentedControlSegmentAlignmentBothSides,
};

/// 渐变色方向的枚举
typedef NS_ENUM(NSUInteger, GZCGradientColorDirection) {
    /// 从(from)右往(to)左
    GZCGradientColorDirectionLeft,
    /// 从左往右
    GZCGradientColorDirectionRight,
    /// 从下往上
    GZCGradientColorDirectionUp,
    /// 从上往下
    GZCGradientColorDirectionDown,
    /// 从右上到左下
    GZCGradientColorDirectionLowerLeft,
    /// 从右下到左上
    GZCGradientColorDirectionUpperLeft,
    /// 从左下到右上
    GZCGradientColorDirectionRightUpper,
    /// 从左上到右下
    GZCGradientColorDirectionLowerRight,
};

@interface GZCGradientColor : NSObject

/// 开始颜色
@property ( nonatomic, strong ) UIColor *fromColor;

/// 结束颜色
@property ( nonatomic, strong ) UIColor *toColor;

/// 方向
@property ( nonatomic, assign ) GZCGradientColorDirection direction;

/// 初始化方法(渐变色)
+ (instancetype)colorWithFromColor:(UIColor *)fromColor
                           toColor:(UIColor *)toColor
                         direction:(GZCGradientColorDirection)direction;
/// 初始化方法(单色)
+ (instancetype)colorWithSingleColor:(UIColor *)color;


- (CAGradientLayer *)transformationToLayer;

@end

@interface GZCSegmentedControl : UIControl

#pragma mark - 自动设置的属性，公开出来方便外部使用，请不要在外部进行改动，以免造成不测
/// 内容容器
@property ( nonatomic, strong, readonly ) UIScrollView *scrollView;
/// 文字宽度数组
@property ( nonatomic, readonly ) NSArray<NSNumber *> *segmentWidthsArray;
/// 点击区域宽度数组
@property ( nonatomic, readonly ) NSArray<NSNumber *> *segmentEnableWidthsArray;
/// 内容宽度是否超出显示范围
@property ( nonatomic, readonly ) BOOL isExceed;
/// 存储显示文字layer
@property ( nonatomic, readonly ) NSArray<CATextLayer *> *textLayersArray;

#pragma mark - 通用属性
/// 标题数组
@property ( nonatomic, strong ) NSArray<NSString *> *sectionTitles;

/// 标题文字样式
@property ( nonatomic, strong ) NSDictionary *titleTextAttributes;

/// 选中的标题文字样式
@property ( nonatomic, strong ) NSDictionary *selectedTitleTextAttributes;

/// 背景颜色,默认[UIColor whiteColor]
@property ( nonatomic, strong ) UIColor *backgroundColor;

/// 选中的滑块/边框颜色
@property ( nonatomic, strong ) UIColor *selectionIndicatorColor;

/// 数值改变的回调
@property ( nonatomic, copy ) GZCIntegerBlock indexChangeBlock;

/// 样式
//@property ( nonatomic, assign ) GZCSegmentedControlType type;

/// 选中样式,默认为'GZCSegmentedControlSelectionStyleNone',仅在初始化时可设置
@property ( nonatomic, assign, readonly ) GZCSegmentedControlSelectionStyle selectionStyle;

/// 宽度样式,默认为'GZCSegmentedControlSegmentWidthStyleFixed'
@property ( nonatomic, assign ) GZCSegmentedControlSegmentWidthStyle segmentWidthStyle;

/// 对齐方式
@property ( nonatomic, assign ) GZCSegmentedControlSegmentAlignment segmentAligment;

/// 是否可以拖动，默认YES。设置为NO时用户不能左右拖动来查看所有分段（当分段内容超过宽度时）
@property ( nonatomic, assign, getter = isUserDraggable) BOOL userDraggable;

/// 是否可以操作，默认YES。设置为NO时将不可点击（选中分段）
@property ( nonatomic, assign, getter = isTouchEnabled) BOOL touchEnabled;

/// 用户选中时是否使用动画改变选中状态,默认YES
@property ( nonatomic, assign ) BOOL shouldAnimateUserSelection;

/// 当前选中的位置
@property ( nonatomic, assign ) NSInteger selectedSegmentIndex;

/// 扩大响应范围，默认(top:0,left:0,bottom:0,right:0)
@property ( nonatomic, assign ) UIEdgeInsets enlargeEdgeInset;

/// 内容边距，默认(top:0,left:0,bottom:0,right:0)
@property ( nonatomic, assign ) UIEdgeInsets contentInsets;

#pragma mark - 分样式属性
#pragma mark - Indicator
/**
 滑块高度,默认2.f
 适用selectionStyle为
 'GZCSegmentedControlSelectionStyleTextWidthStripe'
 'GZCSegmentedControlSelectionStyleFixedWidthStripe'
 'GZCSegmentedControlSelectionStyleEnableWidthStripe'
 **/
@property ( nonatomic, assign ) CGFloat selectionIndicatorHeight;

/**
 滑块宽度,默认20.f
 适用selectionStyle为
 'GZCSegmentedControlSelectionStyleFixedWidthStripe'
 **/
@property ( nonatomic, assign ) CGFloat selectionIndicatorWidth;

/**
 对滑块进行调整，默认为(top:0,left:0,bottom:0,right:0)
 当selectionStyle为'GZCSegmentedControlSelectionStyleTextWidthStripe':
 top:无效  left:滑块向左延长长度  bottom:滑块距离底边边距  right:滑块向右延长长度
 当selectionStyle为'GZCSegmentedControlSelectionStyleFixedWidthStripe':
 top:无效  left:无效  bottom:滑块距离底边边距  right:无效
 当selectionStyle为'GZCSegmentedControlSelectionStyleEnableWidthStripe':无效
 **/
@property ( nonatomic, assign ) UIEdgeInsets selectionIndicatorEdgeInsets;

#pragma mark - Box
/**
 对边框的圆角进行调整，默认为(top:0,left:0,bottom:0,right:0)
 适用selectionStyle为'GZCSegmentedControlSelectionStyleBox'或'GZCSegmentedControlSelectionStyleBoxFill'
 对应位置为:
 top┈┈┈┈┈┬┈┈┈┈┈right
 | text | text |
 left┈┈┈┈┴┈┈┈┈┈bottom
 **/
@property ( nonatomic, assign ) UIEdgeInsets contentRadiusEdgeInsets;

/**
 选中的背景/填充颜色(渐变)数组,如果都相同只传一个即可，否则需要与title个数相同
 适用selectionStyle为'GZCSegmentedControlSelectionStyleBox'或'GZCSegmentedControlSelectionStyleBoxFill'
 **/
@property ( nonatomic, strong ) NSArray <GZCGradientColor *> *selectedBoxBackgroundColors;
/**
 选中的标题颜色数组
 如果颜色都相同推荐使用selectedTitleTextAttributes,并且该属性置nil
 颜色不同则需要设置与title个数相同的颜色数组
 适用selectionStyle为'GZCSegmentedControlSelectionStyleBox'或'GZCSegmentedControlSelectionStyleBoxFill'
 **/
@property ( nonatomic, strong ) NSArray <UIColor *> *selectedBoxTitleColors;

/**
 边框颜色 默认 [UIColor blackColor]
 selectionStyle为'GZCSegmentedControlSelectionStyleBox'或'GZCSegmentedControlSelectionStyleBoxFill'时，该值作为未选中时的边框颜色
 selectionStyle为'GZCSegmentedControlSelectionStyleNone'时，该值作为整体边框颜色
 **/
@property ( nonatomic, strong ) UIColor *borderColor;

/**
 边框宽度 默认0.f
 适用selectionStyle为
 'GZCSegmentedControlSelectionStyleNone'
 'GZCSegmentedControlSelectionStyleBox'
 'GZCSegmentedControlSelectionStyleBoxFill'
 **/
@property ( nonatomic, assign ) CGFloat borderWidth;

#pragma mark - None
/**
 分割线宽度,默认为0（分割线宽度不会占用内容宽度，请注意使用segmentSpacing控制间距）
 适用selectionStyle为'GZCSegmentedControlSelectionStyleNone'
 **/
@property ( nonatomic, assign ) CGFloat verticalDividerWidth;

/// 分段间距,默认为10
@property ( nonatomic, assign ) CGFloat segmentSpacing;

/**
 分割线颜色 默认[UIColor blackColor]
 适用selectionStyle为'GZCSegmentedControlSelectionStyleNone'
 **/
@property ( nonatomic, strong ) UIColor *verticalDividerColor;

#pragma mark - 初始化方法
+ (instancetype)segmentWithSelectionStyle:(GZCSegmentedControlSelectionStyle)selectionStyle;

- (instancetype)initWithSectionTitles:(NSArray<NSString *> *)sectiontitles;

- (instancetype)initWithSelectionStyle:(GZCSegmentedControlSelectionStyle)selectionStyle;

- (instancetype)initWithSectionTitles:(NSArray<NSString *> *)sectiontitles
                       selectionStyle:(GZCSegmentedControlSelectionStyle)selectionStyle;

#pragma mark - 设置方法
- (void)setSelectedSegmentIndex:(NSUInteger)index animated:(BOOL)animated;
- (void)setSelectedSegmentIndex:(NSUInteger)index animated:(BOOL)animated notify:(BOOL)notify;

- (void)setIndexChangeBlock:(GZCIntegerBlock)indexChangeBlock;

#pragma mark - 子类用到的方法(重写可以修改对应文字样式)
- (NSDictionary *)resultingTitleTextAttributes;
- (NSDictionary *)resultingSelectedTitleTextAttributes;
@end
