//
//  GZCSegmentedIndicatorControl.m
//  GZCKit
//
//  Created by GuoZhongCheng on 2018/7/28.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//

#import "GZCSegmentedIndicatorControl.h"

@interface GZCSegmentedIndicatorControl()

/// 下划线
@property (nonatomic, strong) CALayer *selectionIndicatorStripLayer;

@end

@implementation GZCSegmentedIndicatorControl

- (void)drawRect:(CGRect)rect{
    [super drawRect:rect];
    
    self.selectionIndicatorStripLayer.backgroundColor = self.selectionIndicatorColor.CGColor;
    
    switch (self.selectionStyle) {
        case GZCSegmentedControlSelectionStyleTextWidthStripe:
        case GZCSegmentedControlSelectionStyleFixedWidthStripe:
        case GZCSegmentedControlSelectionStyleEnableWidthStripe:{
            if (!self.selectionIndicatorStripLayer.superlayer) {
                [self.scrollView.layer addSublayer:self.selectionIndicatorStripLayer];
            }
            self.selectionIndicatorStripLayer.frame = [self frameForSelectionIndicator];
            break;
        }
        default:
            break;
    }
}

- (CGRect)frameForSelectionIndicator {
    if (self.selectedSegmentIndex < 0 || self.selectedSegmentIndex >= self.sectionTitles.count) {
        return CGRectZero;
    }
    CGFloat indicatorYOffset = self.bounds.size.height - self.selectionIndicatorHeight + self.selectionIndicatorEdgeInsets.bottom;
    __block CGFloat indicatorXOffset = 0.0f;
    CGFloat indicatorWidth = 0.0f;
    CATextLayer *currentTextLayer = self.textLayersArray[self.selectedSegmentIndex];
    CGFloat enableWidth = self.segmentEnableWidthsArray[self.selectedSegmentIndex].floatValue;
    
    switch (self.selectionStyle) {
        case GZCSegmentedControlSelectionStyleTextWidthStripe:{
            indicatorXOffset = currentTextLayer.frame.origin.x;
            indicatorWidth = currentTextLayer.frame.size.width;
            break;
        }
        case GZCSegmentedControlSelectionStyleFixedWidthStripe:{
            indicatorWidth = self.selectionIndicatorWidth;
            indicatorXOffset = currentTextLayer.frame.origin.x + (currentTextLayer.frame.size.width - indicatorWidth)/2.f;
            break;
        }
        case GZCSegmentedControlSelectionStyleEnableWidthStripe:{
            indicatorWidth = enableWidth;
            [self.segmentEnableWidthsArray enumerateObjectsUsingBlock:^(NSNumber * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (idx < self.selectedSegmentIndex) {
                    indicatorXOffset += obj.floatValue;
                }
            }];
            break;
        }
        default:
            break;
    }
    return CGRectMake(indicatorXOffset + self.selectionIndicatorEdgeInsets.left, indicatorYOffset, indicatorWidth - self.selectionIndicatorEdgeInsets.right, self.selectionIndicatorHeight);
}

#pragma mark - 懒加载
- (CALayer *)selectionIndicatorStripLayer{
    if (!_selectionIndicatorStripLayer) {
        _selectionIndicatorStripLayer = [CALayer layer];
    }
    return _selectionIndicatorStripLayer;
}

@end
