//
//  GZCSegmentControl.m
//  GZCKit
//
//  Created by GuoZhongCheng on 2018/7/28.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//

#import "GZCSegmentControl.h"
#import "GZCSegmentedIndicatorControl.h"
#import "GZCSegmentedBoxControl.h"

@implementation GZCGradientColor

+ (instancetype)colorWithFromColor:(UIColor *)fromColor toColor:(UIColor *)toColor direction:(GZCGradientColorDirection)direction{
    GZCGradientColor *color = [GZCGradientColor new];
    color.fromColor = fromColor;
    color.toColor = toColor;
    color.direction = direction;
    return color;
}

+ (instancetype)colorWithSingleColor:(UIColor *)color{
    return [self colorWithFromColor:color toColor:color direction:GZCGradientColorDirectionLeft];
}

- (CAGradientLayer *)transformationToLayer{
    CAGradientLayer * gradientLayer = [CAGradientLayer layer];
    gradientLayer.colors            = @[(__bridge id)self.fromColor.CGColor,
                                        (__bridge id)self.toColor.CGColor];
    switch (self.direction) {
        case GZCGradientColorDirectionLeft:{
            gradientLayer.startPoint        = CGPointMake(1, 0);
            gradientLayer.endPoint          = CGPointMake(0, 0);
            break;
        }
        case GZCGradientColorDirectionRight:{
            gradientLayer.startPoint        = CGPointMake(0, 0);
            gradientLayer.endPoint          = CGPointMake(1, 0);
            break;
        }
        case GZCGradientColorDirectionUp:{
            gradientLayer.startPoint        = CGPointMake(0, 1);
            gradientLayer.endPoint          = CGPointMake(0, 0);
            break;
        }
        case GZCGradientColorDirectionDown:{
            gradientLayer.startPoint        = CGPointMake(0, 0);
            gradientLayer.endPoint          = CGPointMake(0, 1);
            break;
        }
        case GZCGradientColorDirectionLowerLeft:{
            gradientLayer.startPoint        = CGPointMake(1, 0);
            gradientLayer.endPoint          = CGPointMake(0, 1);
            break;
        }
        case GZCGradientColorDirectionUpperLeft:{
            gradientLayer.startPoint        = CGPointMake(1, 1);
            gradientLayer.endPoint          = CGPointMake(0, 0);
            break;
        }
        case GZCGradientColorDirectionLowerRight:{
            gradientLayer.startPoint        = CGPointMake(0, 0);
            gradientLayer.endPoint          = CGPointMake(1, 1);
            break;
        }
        case GZCGradientColorDirectionRightUpper:{
            gradientLayer.startPoint        = CGPointMake(0, 1);
            gradientLayer.endPoint          = CGPointMake(1, 0);
            break;
        }
    }
    gradientLayer.locations = @[@(0),@(1)];
    return gradientLayer;
}

@end

@interface GZCScrollView : UIScrollView
@end

@interface GZCSegmentedControl ()

@property ( nonatomic, assign ) GZCSegmentedControlSelectionStyle selectionStyle;

@property ( nonatomic, assign ) CGFloat segmentWidth;
@property ( nonatomic, strong ) NSArray<NSNumber *> *segmentWidthsArray;
@property ( nonatomic, strong ) NSArray<NSNumber *> *segmentEnableWidthsArray;

/// 存储文字layer
@property ( nonatomic, strong ) NSArray<CATextLayer *> *textLayersArray;

@property ( nonatomic, strong ) GZCScrollView *scrollView;

/// 是否超出范围
@property ( nonatomic, assign ) BOOL isExceed;

/// 文字总长度(包含间距)
@property ( nonatomic, assign ) CGFloat totalTitleWidth;

@end

@implementation GZCScrollView

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if (!self.dragging) {
        [self.nextResponder touchesBegan:touches withEvent:event];
    } else {
        [super touchesBegan:touches withEvent:event];
    }
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    if (!self.dragging) {
        [self.nextResponder touchesMoved:touches withEvent:event];
    } else{
        [super touchesMoved:touches withEvent:event];
    }
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if (!self.dragging) {
        [self.nextResponder touchesEnded:touches withEvent:event];
    } else {
        [super touchesEnded:touches withEvent:event];
    }
}

@end

@implementation GZCSegmentedControl

#pragma mark - 初始化
+ (instancetype)segmentWithSelectionStyle:(GZCSegmentedControlSelectionStyle)selectionStyle{
    GZCSegmentedControl *segmentControl;
    switch (selectionStyle) {
        case GZCSegmentedControlSelectionStyleTextWidthStripe:
        case GZCSegmentedControlSelectionStyleFixedWidthStripe:
        case GZCSegmentedControlSelectionStyleEnableWidthStripe:
        {
            segmentControl = [[GZCSegmentedIndicatorControl alloc] initWithFrame:CGRectZero];
            segmentControl.selectionStyle = selectionStyle;
            break;
        }
        case GZCSegmentedControlSelectionStyleBox:
        case GZCSegmentedControlSelectionStyleBoxFill:
        {
            segmentControl = [[GZCSegmentedBoxControl alloc] initWithFrame:CGRectZero];
            segmentControl.selectionStyle = selectionStyle;
            break;
        }
        case GZCSegmentedControlSelectionStyleNone:
        {
            segmentControl = [[GZCSegmentedControl alloc] initWithFrame:CGRectZero];
            segmentControl.selectionStyle = selectionStyle;
            break;
        }
    }
    return segmentControl;
}
#pragma mark --
- (instancetype)initWithSelectionStyle:(GZCSegmentedControlSelectionStyle)selectionStyle{
    self = [GZCSegmentedControl segmentWithSelectionStyle:selectionStyle];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithSectionTitles:(NSArray<NSString *> *)sectiontitles {
    self = [super initWithFrame:CGRectZero];
    if (self) {
        [self commonInit];
        self.sectionTitles = sectiontitles;
        self.selectionStyle = GZCSegmentedControlSelectionStyleNone;
    }
    return self;
}

- (id)initWithSectionTitles:(NSArray<NSString *> *)sectiontitles selectionStyle:(GZCSegmentedControlSelectionStyle)selectionStyle{
    self = [GZCSegmentedControl segmentWithSelectionStyle:selectionStyle];
    if (self) {
        [self commonInit];
        self.sectionTitles = sectiontitles;
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
        self.selectionStyle = GZCSegmentedControlSelectionStyleNone;
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
        self.selectionStyle = GZCSegmentedControlSelectionStyleNone;
    }
    return self;
}


- (void)commonInit {
    [self addSubview:self.scrollView];
    self.opaque = NO;
    
    self.backgroundColor = [UIColor whiteColor];
    self.selectionIndicatorColor = [UIColor colorWithRed:52.0f/255.0f green:181.0f/255.0f blue:229.0f/255.0f alpha:1.0f];
    self.verticalDividerColor = [UIColor blackColor];
    self.borderColor = [UIColor blackColor];
    self.contentInsets = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
    self.enlargeEdgeInset = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
    self.contentRadiusEdgeInsets = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
    self.selectedSegmentIndex = 0;
    self.segmentSpacing = 10.f;
    self.selectionIndicatorHeight = 2.0f;
    self.selectionIndicatorWidth = 20.f;
    self.selectionIndicatorEdgeInsets = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
    self.segmentWidthStyle = GZCSegmentedControlSegmentWidthStyleFixed;
    self.userDraggable = YES;
    self.touchEnabled = YES;
    self.verticalDividerWidth = 0.0f;
    self.borderWidth = 0.f;
    
    self.shouldAnimateUserSelection = YES;
}

#pragma mark - setter
- (void)layoutSubviews{
    [super layoutSubviews];
    [self updateSegmentsRects];
}

- (void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    [self updateSegmentsRects];
}

- (void)willMoveToSuperview:(UIView *)newSuperview {
    if (newSuperview == nil)
        return;
    
    if (self.sectionTitles) {
        [self updateSegmentsRects];
    }
}

- (void)setSectionTitles:(NSArray<NSString *> *)sectionTitles {
    _sectionTitles = sectionTitles;
    
    [self setNeedsLayout];
    [self setNeedsDisplay];
}

- (void)setSegmentWidthStyle:(GZCSegmentedControlSegmentWidthStyle)segmentWidthStyle{
    _segmentWidthStyle = segmentWidthStyle;
    
    [self setNeedsLayout];
    [self setNeedsDisplay];
}

- (void)setSegmentAligment:(GZCSegmentedControlSegmentAlignment)segmentAligment{
    _segmentAligment = segmentAligment;
    
    [self setNeedsLayout];
    [self setNeedsDisplay];
}

#pragma mark - 事件
#pragma mark - Touch
- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event{
    if (!self.isTouchEnabled){
        return nil;
    }
    CGPoint touchLocation = point;
    CGRect enlargeRect =   CGRectMake(self.bounds.origin.x - self.enlargeEdgeInset.left,
                                      self.bounds.origin.y - self.enlargeEdgeInset.top,
                                      self.bounds.size.width + self.enlargeEdgeInset.left + self.enlargeEdgeInset.right,
                                      self.bounds.size.height + self.enlargeEdgeInset.top + self.enlargeEdgeInset.bottom);
    if (CGRectContainsPoint(enlargeRect, touchLocation)) {
        NSInteger segment = 0;
        CGFloat widthLeft = (touchLocation.x + self.scrollView.contentOffset.x - self.scrollView.frame.origin.x);
        if (widthLeft > 0) {
            CGFloat tempWidth = widthLeft;
            CGFloat totalEnableWidth = 0;
            for (NSNumber *width in self.segmentEnableWidthsArray) {
                tempWidth = tempWidth - [width floatValue];
                if (tempWidth > 0){
                    segment++;
                }
                totalEnableWidth += [width floatValue];
            }
            
            if (segment != self.selectedSegmentIndex && segment < [self sectionCount] && widthLeft <= totalEnableWidth) {
                if (self.isTouchEnabled)
                    return self;
            }
        }
    }
    
    if (self.subviews.count > 1) {
        __block UIView *eventView = nil;
        [self.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (CGRectContainsPoint(obj.frame, point) && obj.hidden == NO) {
                eventView = obj;
            }
        }];
        return eventView;
    }
    return nil;
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInView:self];
    CGRect oldRect = self.scrollView.frame;
    CGRect enlargeRect =   CGRectMake(oldRect.origin.x - self.enlargeEdgeInset.left,
                                      oldRect.origin.y - self.enlargeEdgeInset.top,
                                      oldRect.size.width + self.enlargeEdgeInset.left + self.enlargeEdgeInset.right,
                                      oldRect.size.height + self.enlargeEdgeInset.top + self.enlargeEdgeInset.bottom);
    
    if (CGRectContainsPoint(enlargeRect, touchLocation)) {
        NSInteger segment = 0;
        CGFloat widthLeft = (touchLocation.x + self.scrollView.contentOffset.x - oldRect.origin.x);
        CGFloat tempWidth = widthLeft;
        CGFloat totalEnableWidth = 0;
        for (NSNumber *width in self.segmentEnableWidthsArray) {
            tempWidth = tempWidth - [width floatValue];
            if (tempWidth > 0){
                segment++;
            }
            totalEnableWidth += [width floatValue];
        }
        
        if (segment != self.selectedSegmentIndex && segment < [self sectionCount]) {
            if (self.isTouchEnabled)
                [self setSelectedSegmentIndex:segment animated:self.shouldAnimateUserSelection notify:YES];
        }
    }
}

#pragma mark - 改变选中值
- (void)setSelectedSegmentIndex:(NSInteger)index {
    [self setSelectedSegmentIndex:index animated:NO notify:NO];
}

- (void)setSelectedSegmentIndex:(NSUInteger)index animated:(BOOL)animated {
    [self setSelectedSegmentIndex:index animated:animated notify:NO];
}

- (void)setSelectedSegmentIndex:(NSUInteger)index animated:(BOOL)animated notify:(BOOL)notify {
    _selectedSegmentIndex = index;
    [self setNeedsDisplay];
    
    if (index == -1) {
        return;
    }
    [self scrollToSelectedSegmentIndex:animated];
    
    if (notify)
        [self notifyForSegmentChangeToIndex:index];
}

- (void)notifyForSegmentChangeToIndex:(NSInteger)index {
    if (self.superview)
        [self sendActionsForControlEvents:UIControlEventValueChanged];
    
    if (self.indexChangeBlock)
        self.indexChangeBlock(index);
}

#pragma mark - 滚动动画
- (void)scrollToSelectedSegmentIndex:(BOOL)animated {
    if (self.scrollView.contentSize.width < self.frame.size.width) {
        return;
    }
    CGRect rectForSelectedIndex = CGRectZero;
    __block CGFloat selectedSegmentOffset = 0;
    __block CGFloat selectedSegmentWidth = 0;
    __block CGFloat offsetter = 0;
    [self.segmentEnableWidthsArray enumerateObjectsUsingBlock:^(NSNumber * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (idx < self.selectedSegmentIndex) {
            selectedSegmentOffset += obj.floatValue + self.segmentSpacing;
        }
        if (idx == self.selectedSegmentIndex) {
            selectedSegmentOffset -= obj.floatValue/2.f;
            selectedSegmentWidth = obj.floatValue;
            offsetter = (CGRectGetWidth(self.frame) / 2) - ([obj floatValue] / 2);
        }
    }];
    rectForSelectedIndex = CGRectMake(selectedSegmentOffset - offsetter, 0, selectedSegmentWidth + offsetter*2.f, self.frame.size.height);
    [self.scrollView scrollRectToVisible:rectForSelectedIndex animated:animated];
}

#pragma mark - 绘制
- (void)drawRect:(CGRect)rect{
    [self.backgroundColor setFill];
    UIRectFill([self bounds]);
    self.scrollView.layer.sublayers = nil;
    self.textLayersArray = nil;
    CGRect oldRect = self.scrollView.bounds;
    NSMutableArray *mutEnableArray = [NSMutableArray array];
    NSMutableArray *mutLayerArray = [NSMutableArray array];
    [self.sectionTitles enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        CGFloat stringWidth = 0;
        CGFloat stringHeight = 0;
        CGSize size = [self measureTitleAtIndex:idx];
        stringWidth = size.width;
        stringHeight = size.height;
        CGRect rectDiv = CGRectZero;
        CGRect fullRect = CGRectZero;
        
        CGFloat y = roundf(CGRectGetHeight(self.frame) / 2 - stringHeight / 2) ;
        
        CGRect rect;
        CGFloat xOffset = 0;
        CGFloat enableWidth = 0;
        if (self.isExceed) {
            // 超出宽度
            switch (self.segmentWidthStyle) {
                case GZCSegmentedControlSegmentWidthStyleFixed:{
                    if (idx) {
                        xOffset += (self.segmentSpacing + self.segmentWidth) * idx;
                    }
                    rect = CGRectMake(xOffset + (self.segmentWidth - stringWidth) / 2, y, stringWidth, stringHeight);
                    rectDiv = CGRectMake(xOffset - self.verticalDividerWidth/2 - self.segmentSpacing/2, self.selectionIndicatorHeight * 2, self.verticalDividerWidth, self.frame.size.height - (self.selectionIndicatorHeight * 4));
                    enableWidth += self.segmentWidth + self.segmentSpacing;
                    if (idx == 0 || idx == self.sectionTitles.count - 1) {
                        enableWidth -= self.segmentSpacing/2;
                    }
                    break;
                }
                case GZCSegmentedControlSegmentWidthStyleDynamic:
                case GZCSegmentedControlSegmentWidthStyleEquable:{
                    NSInteger i = 0;
                    for (NSNumber *width in self.segmentWidthsArray) {
                        if (idx == i)
                            break;
                        xOffset = xOffset + [width floatValue] + self.segmentSpacing;
                        i++;
                    }
                    
                    CGFloat widthForIndex = [[self.segmentWidthsArray objectAtIndex:idx] floatValue];
                    rect = CGRectMake(xOffset, y, widthForIndex, stringHeight);
                    fullRect = CGRectMake(self.segmentWidth * idx, 0, widthForIndex, oldRect.size.height);
                    rectDiv = CGRectMake(xOffset - self.verticalDividerWidth/2 - self.segmentSpacing/2, self.selectionIndicatorHeight * 2, self.verticalDividerWidth, self.frame.size.height - (self.selectionIndicatorHeight * 4));
                    enableWidth = widthForIndex + self.segmentSpacing;
                    if (idx == 0 || idx == self.sectionTitles.count - 1) {
                        enableWidth -= self.segmentSpacing/2;
                    }
                    break;
                }
            }
        }else{
            switch (self.segmentAligment) {
                case GZCSegmentedControlSegmentAlignmentCenter:
                case GZCSegmentedControlSegmentAlignmentRight:
                case GZCSegmentedControlSegmentAlignmentLeft:{
                    switch (self.segmentWidthStyle) {
                        case GZCSegmentedControlSegmentWidthStyleFixed:
                        case GZCSegmentedControlSegmentWidthStyleEquable:{
                            if (idx) {
                                xOffset += (self.segmentSpacing + self.segmentWidth ) * idx;
                            }
                            rect = CGRectMake(xOffset + (self.segmentWidth - stringWidth) / 2, y, stringWidth, stringHeight);
                            rectDiv = CGRectMake(xOffset - self.verticalDividerWidth/2 - self.segmentSpacing/2, self.selectionIndicatorHeight * 2, self.verticalDividerWidth, self.frame.size.height - (self.selectionIndicatorHeight * 4));
                            enableWidth += self.segmentWidth + self.segmentSpacing;
                            if (idx == 0 || idx == self.sectionTitles.count - 1) {
                                enableWidth -= self.segmentSpacing/2;
                            }
                            break;
                        }
                        case GZCSegmentedControlSegmentWidthStyleDynamic:{
                            NSInteger i = 0;
                            for (NSNumber *width in self.segmentWidthsArray) {
                                if (idx == i)
                                    break;
                                xOffset = xOffset + [width floatValue] + self.segmentSpacing;
                                i++;
                            }
                            
                            CGFloat widthForIndex = [[self.segmentWidthsArray objectAtIndex:idx] floatValue];
                            rect = CGRectMake(xOffset, y, widthForIndex, stringHeight);
                            rectDiv = CGRectMake(xOffset - self.verticalDividerWidth/2 - self.segmentSpacing/2, self.selectionIndicatorHeight * 2, self.verticalDividerWidth, self.frame.size.height - (self.selectionIndicatorHeight * 4));
                            enableWidth = widthForIndex + self.segmentSpacing;
                            if (idx == 0 || idx == self.sectionTitles.count - 1) {
                                enableWidth -= self.segmentSpacing/2;
                            }
                            break;
                        }
                    }
                    break;
                }
                case GZCSegmentedControlSegmentAlignmentBothSides:{
                    CGFloat widthForIndex = self.segmentWidthsArray[idx].floatValue;
                    CGFloat spacing = (oldRect.size.width - self.totalTitleWidth) / ([self sectionCount] - 1) + self.segmentSpacing;
                    enableWidth = widthForIndex + spacing;
                    if (idx > 0) {
                        NSInteger i = 0;
                        for (NSNumber *width in self.segmentWidthsArray) {
                            if (idx == i)
                                break;
                            xOffset = xOffset + [width floatValue] + spacing ;
                            i++;
                        }
                    }
                    rect = CGRectMake(xOffset, y, widthForIndex, stringHeight);
                    rectDiv = CGRectMake(xOffset - self.verticalDividerWidth/2 - spacing/2, self.selectionIndicatorHeight * 2, self.verticalDividerWidth, self.frame.size.height - (self.selectionIndicatorHeight * 4));
                    if (idx == 0 || idx == self.sectionTitles.count - 1) {
                        enableWidth -= spacing/2;
                    }
                    break;
                }
            }
        }
        rect = CGRectMake(ceilf(rect.origin.x), ceilf(rect.origin.y), ceilf(rect.size.width), ceilf(rect.size.height));
        [mutEnableArray addObject:@(enableWidth)];
        
        CATextLayer *titleLayer = [CATextLayer layer];
        titleLayer.frame = rect;
        titleLayer.alignmentMode = kCAAlignmentCenter;
        if ([UIDevice currentDevice].systemVersion.floatValue < 10.0 ) {
            titleLayer.truncationMode = kCATruncationEnd;
        }
        titleLayer.string = [self attributedTitleAtIndex:idx];
        titleLayer.contentsScale = [[UIScreen mainScreen] scale];
        
        [self.scrollView.layer addSublayer:titleLayer];
        [mutLayerArray addObject:titleLayer];
        
        // Vertical Divider
        if (self.selectionStyle == GZCSegmentedControlSelectionStyleNone
            && idx > 0
            && self.verticalDividerWidth > 0) {
            CALayer *verticalDividerLayer = [CALayer layer];
            verticalDividerLayer.frame = rectDiv;
            verticalDividerLayer.backgroundColor = self.verticalDividerColor.CGColor;
            
            [self.scrollView.layer addSublayer:verticalDividerLayer];
        }
    }];
    self.segmentEnableWidthsArray = [mutEnableArray copy];
    self.textLayersArray = [mutLayerArray copy];
}

#pragma mark - 计算
- (void)updateSegmentsRects {
    self.scrollView.contentInset = UIEdgeInsetsZero;
    self.scrollView.frame = CGRectMake(0, 0, CGRectGetWidth(self.frame), CGRectGetHeight(self.frame));
    
    if (self.selectionStyle == GZCSegmentedControlSelectionStyleNone
        && self.borderWidth > 0) {
        self.layer.borderWidth = self.borderWidth;
        self.layer.borderColor = self.borderColor.CGColor;
    }else{
        self.layer.borderWidth = 0.f;
    }
    
    if ([self sectionCount] > 0) {
        self.segmentWidth = (self.frame.size.width - self.segmentSpacing * ([self sectionCount] - 1)) / [self sectionCount];
    }
    
    __block CGFloat totalWidth = 0.0;
    
    if (self.segmentAligment == GZCSegmentedControlSegmentAlignmentBothSides) {
        totalWidth = [self updateWidthsArray];
    }else{
        switch (self.segmentWidthStyle) {
            case GZCSegmentedControlSegmentWidthStyleFixed:{
                [self.sectionTitles enumerateObjectsUsingBlock:^(id titleString, NSUInteger idx, BOOL *stop) {
                    CGFloat stringWidth = [self measureTitleAtIndex:idx].width;
                    self.segmentWidth = MAX(stringWidth, self.segmentWidth);
                }];
                totalWidth = (self.segmentWidth + self.segmentSpacing) * [self sectionCount] - self.segmentSpacing;
                break;
            }
            case GZCSegmentedControlSegmentWidthStyleDynamic:{
                totalWidth = [self updateWidthsArray];
                break;
            }
            case GZCSegmentedControlSegmentWidthStyleEquable:{
                __block BOOL shouldChange = NO;
                [self.sectionTitles enumerateObjectsUsingBlock:^(id titleString, NSUInteger idx, BOOL *stop) {
                    CGFloat stringWidth = [self measureTitleAtIndex:idx].width;
                    shouldChange = self.segmentWidth < stringWidth;
                }];
                if (shouldChange) {
                    totalWidth = [self updateWidthsArray];
                }else{
                    totalWidth = (self.segmentWidth + self.segmentSpacing) * [self sectionCount] - self.segmentSpacing;
                }
                break;
            }
        }
    }
    self.totalTitleWidth = totalWidth;
    self.scrollView.scrollEnabled = self.isUserDraggable;
    self.scrollView.contentSize = CGSizeMake(totalWidth, self.frame.size.height);
    self.isExceed = totalWidth > self.frame.size.width - self.contentInsets.left - self.contentInsets.right;
    if (!_isExceed) {
        CGFloat x;
        CGFloat width;
        switch (self.segmentAligment) {
            case GZCSegmentedControlSegmentAlignmentCenter:{
                x = (self.frame.size.width - totalWidth) / 2.f + self.contentInsets.left - self.contentInsets.right;
                width = totalWidth;
                break;
            }
            case GZCSegmentedControlSegmentAlignmentLeft:{
                x = self.contentInsets.left;
                width = totalWidth;
                break;
            }
            case GZCSegmentedControlSegmentAlignmentRight:{
                x = self.frame.size.width - totalWidth - self.contentInsets.right;
                width = totalWidth;
                break;
            }
            case GZCSegmentedControlSegmentAlignmentBothSides:{
                x = self.contentInsets.left;
                width = self.frame.size.width - self.contentInsets.left - self.contentInsets.right;
                break;
            }
        }
        self.scrollView.frame = CGRectMake(x, 0, width, self.frame.size.height);
    }else{
        self.scrollView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    }
}

- (CGFloat)updateWidthsArray{
    __block CGFloat totalWidth = 0;
    NSMutableArray *mutableSegmentWidths = [NSMutableArray array];
    [self.sectionTitles enumerateObjectsUsingBlock:^(id titleString, NSUInteger idx, BOOL *stop) {
        CGFloat stringWidth = [self measureTitleAtIndex:idx].width;
        totalWidth += stringWidth + self.segmentSpacing;
        [mutableSegmentWidths addObject:@(stringWidth)];
    }];
    self.segmentWidthsArray = [mutableSegmentWidths copy];
    totalWidth -= self.segmentSpacing;
    return totalWidth;
}

- (NSUInteger)sectionCount {
    return self.sectionTitles.count;
}

- (NSAttributedString *)attributedTitleAtIndex:(NSUInteger)index {
    id title = self.sectionTitles[index];
    BOOL selected = (index == self.selectedSegmentIndex) ? YES : NO;
    if ([title isKindOfClass:[NSAttributedString class]]) {
        return (NSAttributedString *)title;
    } else {
        NSDictionary *titleAttrs = selected ? [self resultingSelectedTitleTextAttributes] : [self resultingTitleTextAttributes];
        UIColor *titleColor = titleAttrs[NSForegroundColorAttributeName];
        if (titleColor) {
            NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:titleAttrs];
            dict[NSForegroundColorAttributeName] = titleColor;
            titleAttrs = [NSDictionary dictionaryWithDictionary:dict];
        }
        return [[NSAttributedString alloc] initWithString:(NSString *)title attributes:titleAttrs];
    }
}

- (CGSize)measureTitleAtIndex:(NSUInteger)index {
    if (index >= self.sectionTitles.count) {
        return CGSizeZero;
    }
    
    id title = self.sectionTitles[index];
    CGSize size = CGSizeZero;
    BOOL selected = (index == self.selectedSegmentIndex) ? YES : NO;
    if ([title isKindOfClass:[NSString class]]) {
        NSDictionary *titleAttrs = selected ? [self resultingSelectedTitleTextAttributes] : [self resultingTitleTextAttributes];
        size = [(NSString *)title sizeWithAttributes:titleAttrs];
        size = CGSizeMake(ceil(size.width), ceil(size.height));
    } else if ([title isKindOfClass:[NSAttributedString class]]) {
        size = [(NSAttributedString *)title size];
    } else {
        NSAssert(title == nil, @"Unexpected type of segment title: %@", [title class]);
        size = CGSizeZero;
    }
    return CGRectIntegral((CGRect){CGPointZero, size}).size;
}

#pragma mark - Styling Support

- (NSDictionary *)resultingTitleTextAttributes {
    NSDictionary *defaults = @{
                               NSFontAttributeName : [UIFont systemFontOfSize:19.0f],
                               NSForegroundColorAttributeName : [UIColor blackColor],
                               };
    
    NSMutableDictionary *resultingAttrs = [NSMutableDictionary dictionaryWithDictionary:defaults];
    
    if (self.titleTextAttributes) {
        [resultingAttrs addEntriesFromDictionary:self.titleTextAttributes];
    }
    
    return [resultingAttrs copy];
}

- (NSDictionary *)resultingSelectedTitleTextAttributes {
    NSMutableDictionary *resultingAttrs = [NSMutableDictionary dictionaryWithDictionary:[self resultingTitleTextAttributes]];
    if (self.selectedTitleTextAttributes) {
        [resultingAttrs addEntriesFromDictionary:self.selectedTitleTextAttributes];
    }
    return [resultingAttrs copy];
}

#pragma mark - 懒加载

- (UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [[GZCScrollView alloc]init];
        _scrollView.scrollsToTop = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.showsHorizontalScrollIndicator = NO;
    }
    return _scrollView;
}

@end
