//
//  GZCLineTextField.m
//  GZCKit
//
//  Created by GuoZhongCheng on 2018/9/4.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//

#import "GZCLineTextField.h"

#define kClearButtonSpacing 0

@interface GZCTempTextField : UITextField

@end

@implementation GZCTempTextField

- (BOOL)canResignFirstResponder{
    if ([self.delegate respondsToSelector:@selector(textFieldDidEndEditing:)]) {
        [self.delegate textFieldDidEndEditing:self];
    }
    return YES;
}

@end

@interface GZCLineTextField()<UITextFieldDelegate,CAAnimationDelegate,CALayerDelegate>{
    BOOL isFirstLayout;
    BOOL isInTop;
    BOOL isInAnimation;
    CGPoint bottomPosition;
    CGPoint topPosition;
    CGRect rightViewFrame;
    CGRect placeholderBounds;
    
    UIColor *_lineNormalColor;
    UIColor *_placeholderColor;
}

@property (nonatomic,strong) GZCTempTextField * textfield;  //输入框
@property (nonatomic,strong) CATextLayer * placeholderTextLayer;  //提示文字
@property (nonatomic,strong) CATextLayer * pettyPlaceholderTextLayer;  //改变后的提示文字
@property (nonatomic,strong) CATextLayer * successTextLayer;  //选中提示文字
@property (nonatomic,strong) CATextLayer * errorTextLayer;  //错误提示文字
@property (nonatomic,strong) CALayer *line;  //下划线
/// 清除按钮
@property (nonatomic,strong) UIButton *clearButton;
/// 右侧View
@property (nonatomic,strong) UIView *mRightView;

@end

@implementation GZCLineTextField

@dynamic lineNormalColor,placeholderColor;

#pragma mark - 初始化
- (instancetype)init{
    self = [super init];
    if (self) {
        [self defaultValue];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self) {
        [self defaultValue];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self){
        [self defaultValue];
    }
    return self;
}

- (void)defaultValue{
    self.autocorrectionType = UITextAutocorrectionTypeNo;
    self.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.placeholderScale = 0.8f;
    self.secureTextEntry = NO;
    self.lineHight = 0.5f;
    self.lineRadius = 0.f;
    self.showLine = YES;
    self.lineFocusDoubleHight = YES;
    self.textfield.adjustsFontSizeToFitWidth = YES;
    _rightViewMode = UITextFieldViewModeNever;
    _clearButtonMode = UITextFieldViewModeWhileEditing;
    _status = GZCLineTextFieldStatusNormal;
    _shouldMoveToTopWhenFocus = YES;
    isInTop = NO;
    isInAnimation = NO;
    isFirstLayout = YES;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    if (isFirstLayout) {
        isFirstLayout = NO;
        [self calculationAndUpdateLayout];
    }else{
        [self calculationLayout];
    }
    
    if (self.rightView == nil && self.clearButtonMode != UITextFieldViewModeNever) {
        // 清空按钮
        self.rightView = self.clearButton;
    }
    
    [self reloadRightView];
    [self updateTextLayerAligment];
}

- (void)calculationLayout{
    // 计算提示文字位置
    CGFloat topHeight = self.placeholderFont.lineHeight * self.placeholderScale;
    CGFloat textFieldHeight = CGRectGetHeight(self.frame) - self.contentEdgeInsets.bottom - self.contentEdgeInsets.top - topHeight - self.lineHight * 2.f;
    CGFloat placeholderHeight = self.placeholderFont.lineHeight;
    self.textfield.frame = CGRectMake(self.contentEdgeInsets.left, CGRectGetHeight(self.frame) - self.contentEdgeInsets.bottom - textFieldHeight - self.lineHight * 2.f, CGRectGetWidth(self.frame) - self.contentEdgeInsets.left - self.contentEdgeInsets.right, textFieldHeight);
    CGFloat left = self.contentEdgeInsets.left;
    switch (self.textfield.leftViewMode) {
        case UITextFieldViewModeAlways:
        {
            left += CGRectGetWidth(self.leftView.frame);
            break;
        }
        default:
            break;
    }
    
    CGFloat width = CGRectGetWidth(self.textfield.frame);
    if (self.leftView) {
        width -= CGRectGetWidth(self.leftView.frame);
    }
    if (self.rightView) {
        width -= CGRectGetWidth(self.rightView.frame);
    }
    placeholderBounds = CGRectMake(0, 0, width, placeholderHeight);
    self.placeholderTextLayer.bounds = placeholderBounds;
    if (_errorTextLayer) {
        self.errorTextLayer.bounds = placeholderBounds;
    }
    if (_successTextLayer) {
        self.successTextLayer.bounds = placeholderBounds;
    }
    
    bottomPosition = CGPointMake(left , floorf(CGRectGetMinY(self.textfield.frame) + (textFieldHeight - placeholderHeight)/2.f));
    switch (self.textAlignment) {
        case NSTextAlignmentRight:
        {
            CGFloat offsetX = width * (1 - self.placeholderScale);
            topPosition = CGPointMake(left + offsetX, CGRectGetMinY(self.textfield.frame) - topHeight);
            break;
        }
        case NSTextAlignmentCenter:{
            CGFloat offsetX = width * (1 - self.placeholderScale)/2.f;
            topPosition = CGPointMake(left + offsetX, CGRectGetMinY(self.textfield.frame) - topHeight);
            break;
        }
        default:{
            topPosition = CGPointMake(left, CGRectGetMinY(self.textfield.frame) - topHeight);
            break;
        }
    }
}

- (void)calculationAndUpdateLayout{
    [self calculationLayout];
    if (isInTop) {
        self.placeholderTextLayer.position = topPosition;
    }else{
        self.placeholderTextLayer.position = bottomPosition;
    }
    self.line.frame = CGRectMake(0, CGRectGetHeight(self.frame) - self.lineHight, CGRectGetWidth(self.frame), self.lineHight);
}

- (BOOL)becomeFirstResponder{
    return [self.textfield becomeFirstResponder];
}

- (BOOL)resignFirstResponder{
    return [self.textfield resignFirstResponder];
}

#pragma mark - 事件
- (void)showErrorMessage:(NSString *)errorMessage
                position:(GZCLineTextFieldMessagePosition)position{
    [self endEditing:YES];
    self.errorMessage = errorMessage;
    [self setStatus:GZCLineTextFieldStatusError];
    switch (position) {
        case GZCLineTextFieldMessagePositionTop:{
            self.errorTextLayer.position = topPosition;
            self.errorTextLayer.transform = CATransform3DMakeScale(self.placeholderScale, self.placeholderScale, 1.f);
            break;
        }
        case GZCLineTextFieldMessagePositionCenter:{
            self.errorTextLayer.position = bottomPosition;
            self.errorTextLayer.transform = CATransform3DMakeScale(1.f, 1.f, 1.f);
            break;
        }
        case GZCLineTextFieldMessagePositionBottom:{
            self.errorTextLayer.position = CGPointMake(topPosition.x, self.frame.size.height);
            self.errorTextLayer.transform = CATransform3DMakeScale(self.placeholderScale, self.placeholderScale, 1.f);
            break;
        }
    }
}

- (void)showSuccessMessage:(NSString *)successMessage
                position:(GZCLineTextFieldMessagePosition)position{
    [self endEditing:YES];
    self.successMessage = successMessage;
    [self setStatus:GZCLineTextFieldStatusSuccess];
    switch (position) {
        case GZCLineTextFieldMessagePositionTop:{
            self.successTextLayer.position = topPosition;
            self.successTextLayer.transform = CATransform3DMakeScale(self.placeholderScale, self.placeholderScale, 1.f);
            break;
        }
        case GZCLineTextFieldMessagePositionCenter:{
            self.successTextLayer.position = bottomPosition;
            self.successTextLayer.transform = CATransform3DMakeScale(1.f, 1.f, 1.f);
            break;
        }
        case GZCLineTextFieldMessagePositionBottom:{
            self.errorTextLayer.position = CGPointMake(topPosition.x, self.frame.size.height);
            self.successTextLayer.transform = CATransform3DMakeScale(self.placeholderScale, self.placeholderScale, 1.f);
            break;
        }
    }
}

#pragma mark - 显示文字
- (NSAttributedString *)attributedPlaceholderText {
    NSDictionary *attrDic = @{
                              NSFontAttributeName : self.placeholderFont,
                              NSForegroundColorAttributeName : (self.status == GZCLineTextFieldStatusFocuse ? self.tintFocusColor : self.placeholderColor)
                              };
    return [[NSAttributedString alloc] initWithString:self.placeholder attributes:attrDic];
}

- (NSAttributedString *)attributedPettyPlaceholderText {
    NSDictionary *attrDic = @{
                              NSFontAttributeName : self.placeholderFont,
                              NSForegroundColorAttributeName : (self.status == GZCLineTextFieldStatusFocuse ? self.tintFocusColor : self.placeholderColor)
                              };
    if (!_pettyPlaceholder) {
        _pettyPlaceholder = self.placeholder;
    }
    return [[NSAttributedString alloc] initWithString:_pettyPlaceholder attributes:attrDic];
}

- (NSAttributedString *)attributedSuccessText {
    NSDictionary *attrDic = @{
                              NSFontAttributeName : self.placeholderFont,
                              NSForegroundColorAttributeName : self.tintSuccessColor
                              };
    return [[NSAttributedString alloc] initWithString:self.successMessage attributes:attrDic];
}

- (NSAttributedString *)attributedErrorText {
    NSDictionary *attrDic = @{
                              NSFontAttributeName : self.placeholderFont,
                              NSForegroundColorAttributeName : self.tintErrorColor
                              };
    return [[NSAttributedString alloc] initWithString:self.errorMessage attributes:attrDic];
}


#pragma mark - 动画
- (void)movePlaceholderToTop{
    if (isInTop || isInAnimation) {
        return;
    }
    
    [self.placeholderTextLayer removeAllAnimations];
    if (_pettyPlaceholderTextLayer) {
        [self.pettyPlaceholderTextLayer removeAllAnimations];
    }
    
    //位置移动
    CABasicAnimation *moveAnimation  = [CABasicAnimation animationWithKeyPath:@"position"];
    moveAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    moveAnimation.fromValue =  @(bottomPosition);
    moveAnimation.toValue = @(topPosition);
    
    //缩放动画
    CABasicAnimation *scaleAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    scaleAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    scaleAnimation.fromValue = @(1.f);
    scaleAnimation.toValue = @(self.placeholderScale);
    
    
    CAAnimationGroup *animationGroup = [CAAnimationGroup animation];
    animationGroup.duration = .2f;
    animationGroup.autoreverses = NO;   //是否重播，原动画的倒播
    animationGroup.removedOnCompletion = NO;
    animationGroup.fillMode = kCAFillModeForwards;
    animationGroup.delegate = self;
    [animationGroup setAnimations:@[moveAnimation,scaleAnimation]];
    
    [self.placeholderTextLayer  addAnimation:animationGroup forKey:@"animationTopGroup"];
    if (_pettyPlaceholderTextLayer) {
        [self.pettyPlaceholderTextLayer  addAnimation:animationGroup forKey:@"animationTopGroup"];
    }
}

- (void)movePlaceholderToBottom{
    if (!isInTop || isInAnimation) {
        return;
    }
    
    [self.placeholderTextLayer removeAllAnimations];
    if (_pettyPlaceholderTextLayer) {
        [self.pettyPlaceholderTextLayer removeAllAnimations];
    }
    
    //位置移动
    CABasicAnimation *moveAnimation  = [CABasicAnimation animationWithKeyPath:@"position"];
    moveAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    moveAnimation.fromValue = @(topPosition);
    moveAnimation.toValue = @(bottomPosition);
    
    //缩放动画
    CABasicAnimation *scaleAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    scaleAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    scaleAnimation.fromValue = @(self.placeholderScale);
    scaleAnimation.toValue = @(1.f);
    
    CAAnimationGroup *animationGroup = [CAAnimationGroup animation];
    animationGroup.duration = .2f;
    animationGroup.autoreverses = NO;   //是否重播，原动画的倒播
    animationGroup.removedOnCompletion = NO;
    animationGroup.fillMode = kCAFillModeForwards;
    animationGroup.delegate = self;
    [animationGroup setAnimations:@[moveAnimation,scaleAnimation]];
    
    [self.placeholderTextLayer  addAnimation:animationGroup forKey:@"animationBottomGroup"];
    if (_pettyPlaceholderTextLayer) {
        [self.pettyPlaceholderTextLayer  addAnimation:animationGroup forKey:@"animationBottomGroup"];
    }
}

- (void)showClearButton{
    self.clearButton.hidden = NO;
}

- (void)hideClearButton{
    self.clearButton.hidden = YES;
}

- (void)showMRightView{
    if (self.rightView == self.clearButton) {
        return;
    }
    self.rightView.frame = rightViewFrame;
    [self.textfield setNeedsLayout];
    [self.textfield setNeedsDisplay];
}

- (void)hideMRightView{
    if (self.rightView == self.clearButton) {
        return;
    }
    if (self.clearButtonMode == UITextFieldViewModeWhileEditing || self.clearButtonMode == UITextFieldViewModeAlways) {
        self.rightView.frame = self.clearButton.frame;
    }else{
        self.rightView.frame = CGRectZero;
    }
    [self.textfield setNeedsLayout];
    [self.textfield setNeedsDisplay];
}

#pragma mark - animation Delegate
- (void)animationDidStart:(CAAnimation *)anim{
    isInAnimation = YES;
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag{
    if (anim == [self.placeholderTextLayer animationForKey:@"animationTopGroup"]) {
        isInTop = YES;
    }else
    if (anim == [self.placeholderTextLayer animationForKey:@"animationBottomGroup"]) {
        isInTop = NO;
    }
    isInAnimation = NO;
}

#pragma mark - textfield Delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    BOOL shouldBegin = YES;
    if ([self.delegate respondsToSelector:@selector(textFieldShouldBeginEditing:)]) {
        shouldBegin = [self.delegate textFieldShouldBeginEditing:self.textfield];
    }
    return shouldBegin;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [self setStatus:GZCLineTextFieldStatusFocuse];
    if ([self.delegate respondsToSelector:@selector(textFieldDidBeginEditing:)]) {
        [self.delegate textFieldDidBeginEditing:self.textfield];
    }
    switch (self.clearButtonMode) {
        case UITextFieldViewModeWhileEditing:
        case UITextFieldViewModeAlways:{
            if (textField.text.length > 0) {
                [self showClearButton];
            }else{
                [self hideClearButton];
            }
            break;
        }
        default:{
            [self hideClearButton];
            break;
        }
    }
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    BOOL shouldEnd = YES;
    if ([self.delegate respondsToSelector:@selector(textFieldShouldEndEditing:)]) {
        shouldEnd = [self.delegate textFieldShouldEndEditing:self.textfield];
    }
    return shouldEnd;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    [self setStatus:GZCLineTextFieldStatusNormal];
    if ([self.delegate respondsToSelector:@selector(textFieldDidEndEditing:)]) {
        [self.delegate textFieldDidEndEditing:self.textfield];
    }
    switch (self.clearButtonMode) {
        case UITextFieldViewModeUnlessEditing:
        {
            if (textField.text.length > 0) {
                [self showClearButton];
            }else{
                [self hideClearButton];
            }
            break;
        }
        default:
        {
            [self hideClearButton];
            break;
        }
    }
}
- (void)textFieldDidEndEditing:(UITextField *)textField reason:(UITextFieldDidEndEditingReason)reason NS_AVAILABLE_IOS(10_0){
    if ([self.delegate respondsToSelector:@selector(textFieldDidEndEditing:reason:)]) {
        [self.delegate textFieldDidEndEditing:self.textfield reason:reason];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    BOOL shouldChange = YES;
    if ([self.delegate respondsToSelector:@selector(textField:shouldChangeCharactersInRange:replacementString:)]) {
        shouldChange = [self.delegate textField:self.textfield shouldChangeCharactersInRange:range replacementString:string];
    }
    if (shouldChange) {
        NSMutableString *mutStr = [NSMutableString stringWithString:self.textfield.text];
        [mutStr replaceCharactersInRange:range withString:string];
        [self setText:mutStr];
    }
    switch (self.clearButtonMode) {
        case UITextFieldViewModeWhileEditing:
        case UITextFieldViewModeAlways:{
            if (textField.text.length > 0) {
                [self showClearButton];
            }else{
                [self hideClearButton];
            }
            break;
        }
        default:{
            [self hideClearButton];
            break;
        }
    }
    return NO;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField{
    if ([self.delegate respondsToSelector:@selector(textFieldShouldClear:)]) {
        return [self.delegate textFieldShouldClear:self.textfield];
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if ([self.delegate respondsToSelector:@selector(textFieldShouldReturn:)]) {
        return [self.delegate textFieldShouldReturn:self.textfield];
    }
    return YES;
}

- (void)clearButtonAction{
    BOOL shouldClear = [self textFieldShouldClear:self.textfield];
    if (shouldClear) {
        self.text = @"";
    }
}

#pragma mark - CALayerDelegate 消除隐式动画
- (id<CAAction>)actionForLayer:(CALayer *)layer forKey:(NSString *)event{
    return [NSNull null];
}

#pragma mark - setter / getter
- (void)setTextAlignment:(NSTextAlignment)textAlignment{
    self.textfield.textAlignment = textAlignment;
}

- (void)updateTextLayerAligment{
    switch (self.textAlignment) {
        case NSTextAlignmentLeft:{
            [self setTextLayerAlignment:kCAAlignmentLeft];
            break;
        }
        case NSTextAlignmentCenter:{
            [self setTextLayerAlignment:kCAAlignmentCenter];
            break;
        }
        case NSTextAlignmentRight:{
            [self setTextLayerAlignment:kCAAlignmentRight];
            break;
        }
        case NSTextAlignmentNatural:{
            [self setTextLayerAlignment:kCAAlignmentNatural];
            break;
        }
        case NSTextAlignmentJustified:{
            [self setTextLayerAlignment:kCAAlignmentJustified];
            break;
        }
    }
}

- (void)setTextLayerAlignment:(NSString *)alignment{
    self.placeholderTextLayer.alignmentMode = alignment;
    if (_pettyPlaceholderTextLayer) {
        _pettyPlaceholderTextLayer.alignmentMode = alignment;
    }
    if (_successTextLayer) {
        _successTextLayer.alignmentMode = alignment;
    }
    if (_errorTextLayer) {
        _errorTextLayer.alignmentMode = alignment;
    }
}

- (NSTextAlignment)textAlignment{
    return self.textfield.textAlignment;
}

- (void)setBackgroundColor:(UIColor *)backgroundColor{
    [super setBackgroundColor:backgroundColor];
    if (_errorTextLayer) {
        self.errorTextLayer.backgroundColor = backgroundColor.CGColor;
    }
    if (_successTextLayer) {
        self.successTextLayer.backgroundColor = backgroundColor.CGColor;
    }
}

- (void)setKeyboardType:(UIKeyboardType)keyboardType{
    self.textfield.keyboardType = keyboardType;
}

- (UIKeyboardType)keyboardType{
    return self.textfield.keyboardType;
}

- (void)setTintColor:(UIColor *)tintColor{
    [super setTintColor:tintColor];
    self.textfield.tintColor = tintColor;
}

- (void)setAutocorrectionType:(UITextAutocorrectionType)autocorrectionType{
    self.textfield.autocorrectionType = autocorrectionType;
}

- (UITextAutocorrectionType)autocorrectionType{
    return self.textfield.autocorrectionType;
}

- (void)setAutocapitalizationType:(UITextAutocapitalizationType)autocapitalizationType{
    self.textfield.autocapitalizationType = autocapitalizationType;
}

- (UITextAutocapitalizationType)autocapitalizationType{
    return self.textfield.autocapitalizationType;
}

- (void)setStatus:(GZCLineTextFieldStatus)status{
    _status = status;
    if (_successTextLayer) {
        self.successTextLayer.opacity = 0.f;
    }
    if (_errorTextLayer) {
        self.errorTextLayer.opacity = 0.f;
    }
    CGFloat lineheight = self.lineHight * (self.lineFocusDoubleHight ? 2 : 1);
    switch (_status) {
        case GZCLineTextFieldStatusNormal:{
            self.placeholderTextLayer.string = [self attributedPlaceholderText];
            if (_pettyPlaceholderTextLayer) {
                self.pettyPlaceholderTextLayer.string = [self attributedPettyPlaceholderText];
            }
            self.line.backgroundColor = self.lineNormalColor.CGColor;
            self.line.frame = CGRectMake(0, CGRectGetHeight(self.frame) - self.lineHight, CGRectGetWidth(self.frame), self.lineHight);
            break;
        }
        case GZCLineTextFieldStatusFocuse:{
            self.placeholderTextLayer.string = [self attributedPlaceholderText];
            if (_pettyPlaceholderTextLayer) {
                self.pettyPlaceholderTextLayer.string = [self attributedPettyPlaceholderText];
            }
            self.line.backgroundColor = self.lineFocusColor.CGColor;
            self.line.frame = CGRectMake(0, CGRectGetHeight(self.frame) - lineheight, CGRectGetWidth(self.frame), lineheight);
            break;
        }
        case GZCLineTextFieldStatusError:{
            self.placeholderTextLayer.opacity = 0.f;
            self.errorTextLayer.opacity = 1.f;
            self.line.backgroundColor = self.lineErrorColor.CGColor;
            self.line.frame = CGRectMake(0, CGRectGetHeight(self.frame) - lineheight, CGRectGetWidth(self.frame), lineheight);
            break;
        }
        case GZCLineTextFieldStatusSuccess:{
            self.placeholderTextLayer.opacity = 0.f;
            self.successTextLayer.opacity = 1.f;
            self.line.backgroundColor = self.lineSuccessColor.CGColor;
            self.line.frame = CGRectMake(0, CGRectGetHeight(self.frame) - lineheight, CGRectGetWidth(self.frame), lineheight);
            break;
        }
    }
    [self setText:self.textfield.text];
}

- (void)setText:(NSString *)text{
    NSString *oldString = self.textfield.text;
    self.textfield.text = text;
    if (text.length == 0) {
        [self hideClearButton];
    }
    if (_shouldMoveToTopWhenFocus) {
        if (text.length > 0 || self.status == GZCLineTextFieldStatusFocuse) {
            [self movePlaceholderToTop];
            if (_pettyPlaceholderTextLayer) {
                self.placeholderTextLayer.opacity = 0.f;
                self.pettyPlaceholderTextLayer.opacity = 1.f;
            }else{
                self.placeholderTextLayer.opacity = 1.f;
            }
        }else{
            [self movePlaceholderToBottom];
            if (_pettyPlaceholderTextLayer) {
                self.placeholderTextLayer.opacity = 1.f;
                self.pettyPlaceholderTextLayer.opacity = 0.f;
            }else{
                self.placeholderTextLayer.opacity = 1.f;
            }
        }
    }else{
        [self movePlaceholderToBottom];
        if (text.length > 0){
            self.placeholderTextLayer.opacity = 0.f;
            if (_pettyPlaceholderTextLayer) {
                self.pettyPlaceholderTextLayer.opacity = 0.f;
            }else{
                self.placeholderTextLayer.opacity = 0.f;
            }
        }else{
            self.placeholderTextLayer.opacity = 1.f;
            if (_pettyPlaceholderTextLayer) {
                self.pettyPlaceholderTextLayer.opacity = 0.f;
            }else{
                self.placeholderTextLayer.opacity = 0.f;
            }
        }
    }
    if (![oldString isEqualToString:text] && self.textChangedBlock) {
        self.textChangedBlock(text);
    }
}

- (NSString *)text{
    return self.textfield.text;
}

- (void)setLeftView:(UIView *)leftView{
    self.textfield.leftView = leftView;
}

- (UIView *)leftView{
    return self.textfield.leftView;
}

- (void)setLeftViewMode:(UITextFieldViewMode)leftViewMode{
    self.textfield.leftViewMode = leftViewMode;
}

- (UITextFieldViewMode)leftViewMode{
    return self.textfield.leftViewMode;
}

- (void)setClearButtonMode:(UITextFieldViewMode)clearButtonMode{
    _clearButtonMode = clearButtonMode;
    if (self.mRightView != nil && _clearButtonMode != UITextFieldViewModeNever) {
        self.textfield.rightView = nil;
        CGSize rightViewSize = self.mRightView.bounds.size;
        CGSize clearBtnSize = self.clearButton.bounds.size;
        CGFloat height = MAX(rightViewSize.height, clearBtnSize.height);
        UIView *realRightView = [[UIView alloc]init];
        realRightView.clipsToBounds = YES;
        [realRightView addSubview:self.mRightView];
        self.mRightView.frame = CGRectMake(clearBtnSize.width + kClearButtonSpacing, height/2.f - rightViewSize.height/2.f, rightViewSize.width, rightViewSize.height);
        if (self.clearButton.superview) {
            [self.clearButton removeFromSuperview];
        }
        [realRightView addSubview:self.clearButton];
        self.clearButton.frame = CGRectMake(0, height/2.f - clearBtnSize.height/2.f, clearBtnSize.width, clearBtnSize.height);
        realRightView.frame = CGRectMake(0, 0, clearBtnSize.width + kClearButtonSpacing + rightViewSize.width, height);
        self.textfield.rightView = realRightView;
        self.textfield.rightViewMode = UITextFieldViewModeAlways;
        rightViewFrame = realRightView.frame;
    }
}

- (void)setRightView:(UIView *)rightView{
    if (self.clearButtonMode != UITextFieldViewModeNever) {
        if (rightView == self.clearButton) {
            self.textfield.rightView = rightView;
            self.textfield.rightViewMode = UITextFieldViewModeAlways;
        }else{
            CGSize rightViewSize = rightView.bounds.size;
            CGSize clearBtnSize = self.clearButton.bounds.size;
            CGFloat height = MAX(rightViewSize.height, clearBtnSize.height);
            UIView *realRightView = [[UIView alloc]init];
            realRightView.clipsToBounds = YES;
            [realRightView addSubview:rightView];
            rightView.frame = CGRectMake(clearBtnSize.width + kClearButtonSpacing, height/2.f - rightViewSize.height/2.f, rightViewSize.width, rightViewSize.height);
            if (self.clearButton.superview) {
                [self.clearButton removeFromSuperview];
            }
            [realRightView addSubview:self.clearButton];
            self.clearButton.frame = CGRectMake(0, height/2.f - clearBtnSize.height/2.f, clearBtnSize.width, clearBtnSize.height);
            realRightView.frame = CGRectMake(0, 0, clearBtnSize.width + kClearButtonSpacing + rightViewSize.width, height);
            self.textfield.rightView = realRightView;
            self.textfield.rightViewMode = UITextFieldViewModeAlways;
            rightViewFrame = realRightView.frame;
        }
    }else{
        self.textfield.rightView = rightView;
        self.mRightView = rightView;
        self.textfield.rightViewMode = UITextFieldViewModeAlways;
        rightViewFrame = rightView.frame;
    }
}

- (void)reloadRightView{
    if (self.rightView == nil || self.rightView == self.clearButton) {
        return;
    }
    __block CGFloat totalWidth = kClearButtonSpacing;
    __block CGFloat totalHeight = 0;
    [self.rightView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        totalWidth += CGRectGetWidth(obj.frame);
        totalHeight = MAX(totalHeight, CGRectGetHeight(obj.frame));
    }];
    rightViewFrame = CGRectMake(0, 0, totalWidth, totalHeight);
    switch (self.rightViewMode) {
        case UITextFieldViewModeAlways:{
            [self showMRightView];
            break;
        }
        case UITextFieldViewModeWhileEditing:{
            if (self.status == GZCLineTextFieldStatusFocuse) {
                [self showMRightView];
            }else{
                [self hideMRightView];
            }
            break;
        }
        case UITextFieldViewModeUnlessEditing:{
            if (self.status != GZCLineTextFieldStatusFocuse) {
                [self showMRightView];
            }else{
                [self hideMRightView];
            }
            break;
        }
        case UITextFieldViewModeNever:{
            [self hideMRightView];
            break;
        }
    }
}

- (UIView *)rightView{
    return self.textfield.rightView;
}

- (void)setAdjustsFontSizeToFitWidth:(BOOL)adjustsFontSizeToFitWidth{
    self.textfield.adjustsFontSizeToFitWidth = adjustsFontSizeToFitWidth;
}

- (BOOL)adjustsFontSizeToFitWidth{
    return self.textfield.adjustsFontSizeToFitWidth;
}

- (void)setClearImage:(UIImage *)clearImage{
    _clearImage = [clearImage copy];
    if (_clearImage) {
        [self.clearButton setImage:_clearImage forState:UIControlStateNormal];
        [self.clearButton setImage:_clearImage forState:UIControlStateHighlighted];
    }
}

- (void)setSecureTextEntry:(BOOL)secureTextEntry{
    self.textfield.secureTextEntry = secureTextEntry;
}

- (BOOL)isSecureTextEntry{
    return self.textfield.isSecureTextEntry;
}

- (void)setPlaceholder:(NSString *)placeholder{
    _placeholder = [placeholder copy];
    // 提示文字
    NSAttributedString *placeholderAttr = [self attributedPlaceholderText];
    CGSize placeholderSize = placeholderAttr.size;
    self.placeholderTextLayer.string = placeholderAttr;
    self.placeholderTextLayer.bounds = CGRectMake(0, 0, placeholderSize.width, placeholderSize.height);
}

- (void)setPlaceholderColor:(UIColor *)placeholderColor{
    _placeholderColor = placeholderColor;
    if (self.placeholder.length > 0) {
        self.placeholderTextLayer.string = [self attributedPlaceholderText];
    }
}

- (void)setLineRadius:(CGFloat)lineRadius{
    _lineRadius = lineRadius;
    self.line.cornerRadius = _lineRadius;
}

- (void)setLineNormalColor:(UIColor *)lineNormalColor{
    _lineNormalColor = lineNormalColor;
    self.line.backgroundColor = _lineNormalColor.CGColor;
}

- (void)setPettyPlaceholder:(NSString *)pettyPlaceholder{
    _pettyPlaceholder = [pettyPlaceholder copy];
    // 缩小后的提示文字
    NSAttributedString *pettyPlaceholderAttr = [self attributedPettyPlaceholderText];
    CGSize pettyplaceholderSize = pettyPlaceholderAttr.size;
    self.pettyPlaceholderTextLayer.string = pettyPlaceholderAttr;
    self.pettyPlaceholderTextLayer.bounds = CGRectMake(0, 0, pettyplaceholderSize.width, pettyplaceholderSize.height);
}

- (void)setErrorMessage:(NSString *)errorMessage{
    _errorMessage = [errorMessage copy];
    // 错误提示
    NSAttributedString *errorAttr = [self attributedErrorText];
    CGSize errorSize = errorAttr.size;
    self.errorTextLayer.string = errorAttr;
    self.errorTextLayer.bounds = CGRectMake(0, 0, errorSize.width, errorSize.height);
}

- (void)setSuccessMessage:(NSString *)successMessage{
    _successMessage = [successMessage copy];
    NSAttributedString *successAttr = [self attributedSuccessText];
    CGSize successSize = successAttr.size;
    self.successTextLayer.string = successAttr;
    self.successTextLayer.bounds = CGRectMake(0, 0, successSize.width, successSize.height);
}

#pragma mark - 懒加载
- (UIButton *)clearButton{
    if (!_clearButton) {
        _clearButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _clearButton.frame = CGRectMake(0, 0, 35, 35);
        [_clearButton setImage:[self defaultClearImageWithColor:[UIColor colorWithRed:235/255.f green:236/255.f blue:243/255.f alpha:1.f]] forState:UIControlStateNormal];
        [_clearButton setImage:[self defaultClearImageWithColor:[UIColor lightGrayColor]] forState:UIControlStateHighlighted];
        [_clearButton addTarget:self action:@selector(clearButtonAction) forControlEvents:UIControlEventTouchUpInside];
        _clearButton.contentEdgeInsets = UIEdgeInsetsMake(10, 19, 10, 0);
        _clearButton.hidden = YES;
    }
    return _clearButton;
}

// 绘制清除图片
- (UIImage *)defaultClearImageWithColor:(UIColor *)color
{
    UIImage *newImage = nil;
    CGSize imageSize = CGSizeMake(15, 15);
    CGRect frame = CGRectMake(0, 0, imageSize.width, imageSize.height);
    UIGraphicsBeginImageContextWithOptions(frame.size, NO, [UIScreen mainScreen].scale);
    {
        UIBezierPath *path = [UIBezierPath bezierPath];
        path.lineCapStyle = kCGLineCapRound;
        path.lineWidth = 1.f;
        [path addArcWithCenter:CGPointMake(imageSize.width/2.f, imageSize.height/2.f) radius:imageSize.width/2.f - 0.5f startAngle:0 endAngle:M_PI*2 clockwise:YES];
        [path moveToPoint:CGPointMake(imageSize.width/3.f, imageSize.height/3.f)];
        [path addLineToPoint:CGPointMake(imageSize.width/3.f * 2, imageSize.height/3.f * 2)];
        [path moveToPoint:CGPointMake(imageSize.width/3.f * 2, imageSize.height/3.f)];
        [path addLineToPoint:CGPointMake(imageSize.width/3.f, imageSize.height/3.f * 2)];
        [color setFill];
        [path fill];
        [[UIColor whiteColor] setStroke];
        [path stroke];
        newImage = UIGraphicsGetImageFromCurrentImageContext();
    }
    UIGraphicsEndImageContext();
    return newImage;
}

- (GZCTempTextField *)textfield{
    if (!_textfield) {
        _textfield = [[GZCTempTextField alloc]init];
        _textfield.borderStyle = UITextBorderStyleNone;
        _textfield.textColor = [UIColor blackColor];
        _textfield.font = [UIFont systemFontOfSize:15];
        _textfield.delegate = self;
        [self addSubview:_textfield];
    }
    return _textfield;
}

- (CATextLayer *)placeholderTextLayer{
    if (!_placeholderTextLayer) {
        _placeholderTextLayer = [self defaultTextLayer];
    }
    return _placeholderTextLayer;
}

- (CATextLayer *)pettyPlaceholderTextLayer{
    if (!_pettyPlaceholderTextLayer) {
        _pettyPlaceholderTextLayer = [self defaultTextLayer];
        _pettyPlaceholderTextLayer.opacity = 0.f;
    }
    return _pettyPlaceholderTextLayer;
}

- (CATextLayer *)successTextLayer{
    if (!_successTextLayer) {
        _successTextLayer = [self defaultTextLayer];
        _successTextLayer.backgroundColor = [UIColor whiteColor].CGColor;
        _successTextLayer.delegate = self;
    }
    return _successTextLayer;
}

- (CATextLayer *)errorTextLayer{
    if (!_errorTextLayer) {
        _errorTextLayer = [self defaultTextLayer];
        _errorTextLayer.backgroundColor = [UIColor whiteColor].CGColor;
        _errorTextLayer.delegate = self;
    }
    return _errorTextLayer;
}

- (CALayer *)line{
    if (!_line) {
        _line = [CALayer layer];
        _line.backgroundColor = self.lineNormalColor.CGColor;
        [self.layer addSublayer:_line];
    }
    return _line;
}

- (CATextLayer *)defaultTextLayer{
    CATextLayer *textLayer = [CATextLayer layer];
    textLayer.alignmentMode = kCAAlignmentLeft;
    textLayer.contentsScale = [[UIScreen mainScreen] scale];
    if ([UIDevice currentDevice].systemVersion.floatValue < 10.0 ) {
        textLayer.truncationMode = kCATruncationEnd;
    }
    textLayer.anchorPoint = CGPointMake(0, 0);
    [self.layer addSublayer:textLayer];
    return textLayer;
}

- (UIFont *)placeholderFont{
    if (!_placeholderFont) {
        _placeholderFont = [UIFont systemFontOfSize:15.f];
    }
    return _placeholderFont;
}

- (UIColor *)placeholderColor{
    if (!_placeholderColor) {
        _placeholderColor = [UIColor lightGrayColor];
    }
    return _placeholderColor;
}

- (UIColor *)tintNormalColor{
    if (!_tintNormalColor) {
        _tintNormalColor = [UIColor lightGrayColor];
    }
    return _tintNormalColor;
}

- (UIColor *)tintFocusColor{
    if (!_tintFocusColor) {
        _tintFocusColor = [UIColor lightGrayColor];
    }
    return _tintFocusColor;
}

- (UIColor *)tintErrorColor{
    if (!_tintErrorColor) {
        _tintErrorColor = [UIColor lightGrayColor];
    }
    return _tintErrorColor;
}

- (UIColor *)tintSuccessColor{
    if (!_tintSuccessColor) {
        _tintSuccessColor = [UIColor lightGrayColor];
    }
    return _tintSuccessColor;
}

- (UIColor *)lineNormalColor{
    if (!_lineNormalColor) {
        _lineNormalColor = self.tintNormalColor;
    }
    return _lineNormalColor;
}

- (UIColor *)lineFocusColor{
    if (!_lineFocusColor) {
        _lineFocusColor = self.tintFocusColor;
    }
    return _lineFocusColor;
}

- (UIColor *)lineErrorColor{
    if (!_lineErrorColor) {
        _lineErrorColor = self.tintErrorColor;
    }
    return _lineErrorColor;
}

- (UIColor *)lineSuccessColor{
    if (!_lineSuccessColor) {
        _lineSuccessColor = self.tintSuccessColor;
    }
    return _lineSuccessColor;
}

- (UIFont *)font{
    if (!_font) {
        _font = [UIFont systemFontOfSize:15.f];
    }
    return _font;
}

@end

