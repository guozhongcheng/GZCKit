//
//  GZCLineTextField.h
//  GZCKit
//
//  Created by GuoZhongCheng on 2018/9/4.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, GZCLineTextFieldStatus) {
    /// 普通状态
    GZCLineTextFieldStatusNormal,
    /// 获取焦点状态
    GZCLineTextFieldStatusFocuse,
    /// 显示错误提示
    GZCLineTextFieldStatusError,
    /// 显示成功提示
    GZCLineTextFieldStatusSuccess
};

typedef NS_ENUM(NSUInteger, GZCLineTextFieldMessagePosition) {
    /// 显示在textfield顶部（和提示文字缩小的位置相同）
    GZCLineTextFieldMessagePositionTop,
    /// 显示在textfield中（和没有输入时的提示文字位置相同）,会将已输入的文字遮挡
    GZCLineTextFieldMessagePositionCenter,
    /// 显示在line下方
    GZCLineTextFieldMessagePositionBottom
};

typedef void(^GZCStringBlock)(NSString *string);

IB_DESIGNABLE
@interface GZCLineTextField : UIView

/// 输入文字对其方式 默认NSTextAlignmentLeft
@property ( nonatomic, assign ) NSTextAlignment textAlignment;

/// 选中时是否把提示文字缩小到顶部 默认YES
@property ( nonatomic, assign ) BOOL shouldMoveToTopWhenFocus;
/// 提示信息
@property (nonatomic,copy) NSString * placeholder;
/// 缩小后的提示信息,如果不传则默认使用placeholder
@property ( nonatomic, copy ) NSString *pettyPlaceholder;
/// 提示信息文字颜色，默认lightGray
@property (nonatomic,copy) UIColor * placeholderColor;
/// 提示文字字体，默认15号系统字体
@property (nonatomic,copy) UIFont * placeholderFont;
/// 缩小时提示文字缩放倍数,默认0.8f
@property ( nonatomic, assign ) CGFloat placeholderScale;

/// 输入框文字
@property (nonatomic,copy) NSString *text;
/// 文字字体，默认15号系统字体
@property (nonatomic,copy) UIFont * font;
/// 超出输入框范围时是否自动缩小字体  默认YES
@property ( nonatomic, assign ) BOOL adjustsFontSizeToFitWidth;
/// 文本颜色，默认黑色
@property (nonatomic,copy) UIColor * textColor;

/// 输入框状态
@property ( nonatomic, assign ) GZCLineTextFieldStatus status;
/// 错误信息文字
@property (nonatomic,copy) NSString *errorMessage;
/// 成功信息文字
@property (nonatomic,copy) NSString *successMessage;
/// 未获取焦点时的颜色（提示信息），默认lightGray
@property (nonatomic,copy) UIColor *tintNormalColor;
/// 获取焦点时的颜色（提示信息），默认等于lightGray
@property (nonatomic,copy) UIColor *tintFocusColor;
/// 显示错误信息的颜色（提示信息），默认lightGray
@property (nonatomic,copy) UIColor *tintErrorColor;
/// 显示错误信息的颜色（提示信息），默认lightGray
@property (nonatomic,copy) UIColor *tintSuccessColor;

/// 删除按钮出现时机  默认UITextFieldViewModeWhileEditing
@property ( nonatomic, assign ) UITextFieldViewMode clearButtonMode;
/// 删除按钮图片  不设置则使用系统自带的图片
@property ( nonatomic, copy ) UIImage *clearImage;

/// 键盘样式
@property (nonatomic,assign) UIKeyboardType keyboardType;
/// 输入框距离四周的边距
@property (nonatomic,assign) UIEdgeInsets contentEdgeInsets;

/// 是否显示下划线  默认YES
@property (nonatomic,assign) BOOL showLine;
/// 选中时底部线条是否高度加倍   默认YES
@property (nonatomic,assign) BOOL lineFocusDoubleHight;
/// 底部线条高度   默认0.5f
@property (nonatomic,assign) CGFloat lineHight;
/// 底部线条圆角   默认0.f
@property (nonatomic,assign) CGFloat lineRadius;
/// 未获取焦点时的颜色（下划线），默认tintNormalColor
@property (nonatomic,copy) UIColor *lineNormalColor;
/// 获取焦点时的颜色（下划线），默认tintFocusColor
@property (nonatomic,copy) UIColor *lineFocusColor;
/// 失败提示颜色（下划线），默认tintErrorColor
@property (nonatomic,copy) UIColor *lineErrorColor;
/// 成功提示颜色（下划线），默认tintSuccessColor
@property (nonatomic,copy) UIColor *lineSuccessColor;

/// 输入时是否自动提示  默认UITextAutocorrectionTypeNo
@property (nonatomic,assign) UITextAutocorrectionType autocorrectionType;
/// 是否自动大小写   默认UITextAutocapitalizationTypeNone
@property ( nonatomic, assign ) UITextAutocapitalizationType autocapitalizationType;
/// 是否密码输入框   默认NO
@property (nonatomic, getter = isSecureTextEntry) BOOL secureTextEntry;
/// TextField代理
@property (nonatomic,weak) id<UITextFieldDelegate> delegate;
/// 文字变化回调
@property ( nonatomic, copy ) GZCStringBlock textChangedBlock;

/// textfield左侧view
@property (nonatomic,strong) UIView               *leftView;
/// leftView的显示时机 默认 UITextFieldViewModeNever
@property (nonatomic)        UITextFieldViewMode  leftViewMode;
/// textfield右侧侧view
@property (nonatomic,strong) UIView               *rightView;
/// rightView的显示时机 默认 UITextFieldViewModeNever
@property (nonatomic)        UITextFieldViewMode  rightViewMode;

/// 更新rightview
- (void)reloadRightView;

/**
 显示错误信息（同时会收起键盘）
 @param errorMessage 错误信息
 @param position 显示信息的位置
 */
- (void)showErrorMessage:(NSString *)errorMessage
                position:(GZCLineTextFieldMessagePosition)position;
/**
 显示成功信息（同时会收起键盘）
 @param successMessage 提示信息
 @param position 显示信息的位置
 */
- (void)showSuccessMessage:(NSString *)successMessage
                  position:(GZCLineTextFieldMessagePosition)position;

@end
