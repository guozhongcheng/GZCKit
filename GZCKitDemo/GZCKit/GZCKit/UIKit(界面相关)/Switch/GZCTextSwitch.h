//
//  GZCTextSwitch.h
//  GZCKit
//
//  Created by GuoZhongCheng on 2018/7/30.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^GZCSwitchIndexChangeBlock)(NSInteger index);

@interface GZCTextSwitch : UIControl

/// 是否可点击
@property ( nonatomic, getter = isTouchEnabled) BOOL touchEnabled;

/// 点击是否有动画,默认YES
@property ( nonatomic, getter = isTouchAnimation ) BOOL touchAnimation;

/// 是否根据文字宽度调整宽度,默认YES
@property ( nonatomic, getter = isAutoWidth ) BOOL autoWidth;

/// 弧度
@property ( nonatomic, assign ) CGFloat cornerRadius;

/// 选中的位置
@property ( nonatomic, assign ) NSInteger selectIndex;

/// 文字数组(只支持2个元素)
@property ( nonatomic,   copy ) NSArray <NSString *> *titles;

/// 滑块颜色
@property ( nonatomic, strong ) UIColor *selectionIndicatorColor;
/// 背景颜色
@property ( nonatomic, strong ) UIColor *backgroundColor;

/// 未选中标题文字参数
@property ( nonatomic, strong) NSDictionary *titleTextAttributes;
/// 选中标题文字参数
@property ( nonatomic, strong) NSDictionary *selectedTitleTextAttributes;

/// 事件
@property (nonatomic, copy) GZCSwitchIndexChangeBlock indexChangeBlock;

/// 初始化
- (instancetype)initWithTitles:(NSArray<NSString *> *)titles;
- (instancetype)initWithLeftTitle:(NSString *)leftTitle
                       rightTitle:(NSString *)rightTitle;

@end

