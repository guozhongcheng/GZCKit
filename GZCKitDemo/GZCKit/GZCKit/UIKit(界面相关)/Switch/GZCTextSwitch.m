//
//  GZCTextSwitch.m
//  GZCKit
//
//  Created by GuoZhongCheng on 2018/7/30.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//

#import "GZCTextSwitch.h"

@interface GZCTextSwitch()

/// 滑块
@property ( nonatomic, strong ) CALayer *selectionIndicatorLayer;

/// 左侧文字
@property ( nonatomic, strong ) CATextLayer *leftTitleLayer;
/// 右侧文字
@property ( nonatomic, strong ) CATextLayer *rightTitleLayer;

/// 左侧文字宽度
@property ( nonatomic, assign ) CGFloat leftTitleWidth;
/// 右侧文字宽度
@property ( nonatomic, assign ) CGFloat rightTitleWidth;

@end

@implementation GZCTextSwitch{
    /// 是否在动画过程中
    BOOL isInAnimation;
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        [self commonInit];
    }
    
    return self;
}

- (instancetype)initWithTitles:(NSArray<NSString *> *)titles{
    self = [super initWithFrame:CGRectZero];
    if (self) {
        [self commonInit];
        self.titles = titles;
    }
    return self;
}

- (instancetype)initWithLeftTitle:(NSString *)leftTitle rightTitle:(NSString *)rightTitle{
    self = [super initWithFrame:CGRectZero];
    if (self) {
        [self commonInit];
        self.titles = @[leftTitle,rightTitle];
    }
    return self;
}

- (void)commonInit {
    self.clipsToBounds = YES;
    _backgroundColor = [UIColor whiteColor];
    self.opaque = NO;
    _selectionIndicatorColor = [UIColor colorWithRed:52.0f/255.0f green:181.0f/255.0f blue:229.0f/255.0f alpha:1.0f];
    self.selectIndex = 0;
    self.cornerRadius = 0;
    self.touchAnimation = YES;
    self.autoWidth = YES;
    self.selectionIndicatorLayer.cornerRadius = _cornerRadius;
    self.touchEnabled = YES;
    self.contentMode = UIViewContentModeRedraw;
}

- (void)setTitles:(NSArray<NSString *> *)titles{
    _titles = titles;
    [self updateWidth];
}

- (void)setCornerRadius:(CGFloat)cornerRadius{
    _cornerRadius = cornerRadius;
    self.layer.cornerRadius = cornerRadius;
    self.selectionIndicatorLayer.cornerRadius = _cornerRadius;
    [self updateWidth];
}

#pragma mark - 懒加载
- (CALayer *)selectionIndicatorLayer{
    if (!_selectionIndicatorLayer) {
        _selectionIndicatorLayer = [CALayer layer];
    }
    return _selectionIndicatorLayer;
}

- (CATextLayer *)leftTitleLayer{
    if (!_leftTitleLayer) {
        _leftTitleLayer = [CATextLayer layer];
        _leftTitleLayer.alignmentMode = kCAAlignmentCenter;
        if ([UIDevice currentDevice].systemVersion.floatValue < 10.0 ) {
            _leftTitleLayer.truncationMode = kCATruncationEnd;
        }
        _leftTitleLayer.contentsScale = [[UIScreen mainScreen] scale];
    }
    return _leftTitleLayer;
}

- (CATextLayer *)rightTitleLayer{
    if (!_rightTitleLayer) {
        _rightTitleLayer = [CATextLayer layer];
        _rightTitleLayer.alignmentMode = kCAAlignmentCenter;
        if ([UIDevice currentDevice].systemVersion.floatValue < 10.0 ) {
            _rightTitleLayer.truncationMode = kCATruncationEnd;
        }
        _rightTitleLayer.contentsScale = [[UIScreen mainScreen] scale];
    }
    return _rightTitleLayer;
}

#pragma mark - Index Change
- (void)setSelectIndex:(NSInteger)selectIndex{
    [self setSelectIndex:selectIndex animated:NO notify:NO];
}

- (void)setSelectIndex:(NSUInteger)index animated:(BOOL)animated {
    [self setSelectIndex:index animated:animated notify:NO];
}

- (void)setSelectIndex:(NSUInteger)index animated:(BOOL)animated notify:(BOOL)notify {
    _selectIndex = index;
    [self setNeedsDisplay];
    
    if (index == -1) {
        [self.selectionIndicatorLayer removeFromSuperlayer];
    } else {
        if ( [self.selectionIndicatorLayer superlayer] == nil){
            [self.layer insertSublayer:self.selectionIndicatorLayer atIndex:0];
            [self setSelectIndex:index animated:NO notify:YES];
            return;
        }
        if (animated) {
            if (notify)
                [self notifyForSegmentChangeToIndex:index];
            
            self.selectionIndicatorLayer.actions = nil;
            
            [CATransaction begin];
            [CATransaction setAnimationDuration:0.15f];
            [CATransaction setAnimationTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
            self.selectionIndicatorLayer.frame = [self frameForSelectionIndicator];
            [CATransaction commit];
        } else {
            NSMutableDictionary *newActions = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[NSNull null], @"position", [NSNull null], @"bounds", nil];
            self.selectionIndicatorLayer.actions = newActions;
            self.selectionIndicatorLayer.frame = [self frameForSelectionIndicator];
            
            if (notify)
                [self notifyForSegmentChangeToIndex:index];
        }
    }
}

- (void)notifyForSegmentChangeToIndex:(NSInteger)index {
    if (self.superview)
        [self sendActionsForControlEvents:UIControlEventValueChanged];
    
    if (self.indexChangeBlock)
        self.indexChangeBlock(index);
}

#pragma mark - Animation
- (void)stretchRightAnimation{
    self.selectionIndicatorLayer.actions = nil;
    CGRect oldRect = self.selectionIndicatorLayer.frame;
    CGRect newRect = CGRectMake(CGRectGetMinX(oldRect), CGRectGetMinY(oldRect), CGRectGetWidth(oldRect) + _rightTitleWidth/4.f, CGRectGetHeight(oldRect));
    [CATransaction begin];
    [CATransaction setAnimationDuration:.3f];
    [CATransaction setAnimationTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
    self.selectionIndicatorLayer.frame = newRect;
    [CATransaction commit];
}

- (void)stretchLeftAnimation{
    self.selectionIndicatorLayer.actions = nil;
    CGRect oldRect = self.selectionIndicatorLayer.frame;
    CGRect newRect = CGRectMake(CGRectGetMinX(oldRect) - _leftTitleWidth/4.f, CGRectGetMinY(oldRect), CGRectGetWidth(oldRect) + _leftTitleWidth/4.f, CGRectGetHeight(oldRect));
    [CATransaction begin];
    [CATransaction setAnimationDuration:.3f];
    [CATransaction setAnimationTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
    self.selectionIndicatorLayer.frame = newRect;
    [CATransaction commit];
}

- (void)stretchDefaultAnimation{
    self.selectionIndicatorLayer.actions = nil;
    CGRect newRect = [self frameForSelectionIndicator];
    [CATransaction begin];
    [CATransaction setAnimationDuration:.15f];
    [CATransaction setAnimationTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
    self.selectionIndicatorLayer.frame = newRect;
    [CATransaction commit];
}

#pragma mark - Touch
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    if (!self.isTouchEnabled)
        return;
    NSLog(@"began");
    UITouch *touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInView:self];
    
    CGRect enlargeRect =  self.bounds;
    
    if (CGRectContainsPoint(enlargeRect, touchLocation)) {
        NSInteger index = touchLocation.x > _leftTitleWidth;
        if (index != self.selectIndex && index < [self.titles count]) {
            if (self.isTouchAnimation){
                index ? [self stretchRightAnimation] : [self stretchLeftAnimation];
            }
        }
    }
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInView:self];
    
    CGRect enlargeRect =  self.bounds;
    
    if (CGRectContainsPoint(enlargeRect, touchLocation)) {
        NSInteger index = touchLocation.x > _leftTitleWidth;
        if (index != self.selectIndex && index < [self.titles count]) {
            if (self.isTouchEnabled){
                [self setSelectIndex:index animated:self.isTouchAnimation notify:YES];
            }
        }else{
            [self stretchDefaultAnimation];
        }
    }
}

#pragma mark - Drawing
- (CGSize)intrinsicContentSize{
    return self.bounds.size;
}

- (CGRect)frameForSelectionIndicator {
    return self.selectIndex ?
    CGRectMake( _leftTitleWidth, 0, _rightTitleWidth , CGRectGetHeight(self.frame)):
    CGRectMake(0, 0, _leftTitleWidth , CGRectGetHeight(self.frame));
}

- (void)drawRect:(CGRect)rect {
    [self.backgroundColor setFill];
    UIRectFill([self bounds]);
    
    __weak __typeof(self)weakSelf = self;
    
    self.selectionIndicatorLayer.backgroundColor = self.selectionIndicatorColor.CGColor;
    
    if ([self.leftTitleLayer superlayer] == nil) {
        [self.layer addSublayer:_leftTitleLayer];
    }
    if ([self.rightTitleLayer superlayer] == nil) {
        [self.layer addSublayer:_rightTitleLayer];
    }
    if (!self.isAutoWidth) {
        _leftTitleWidth = CGRectGetWidth(rect)/2.f;
        _rightTitleWidth = _leftTitleWidth;
    }else{
        CGFloat newWidth = self.leftTitleWidth + self.rightTitleWidth;
        if (CGRectGetWidth(rect)!= newWidth) {
            self.bounds = CGRectMake(0, 0, newWidth, CGRectGetHeight(rect));
        }
    }
    
    [self.titles enumerateObjectsUsingBlock:^(id titleString, NSUInteger idx, BOOL *stop) {
        
        CGFloat stringWidth = 0;
        CGFloat stringHeight = 0;
        CGSize size = [self measureTitleAtIndex:idx];
        stringWidth = size.width;
        stringHeight = size.height;
        CGFloat y = roundf(CGRectGetHeight(self.frame) / 2 - stringHeight / 2);
        CGFloat x = 0;
        CGRect rect;
        NSAttributedString *string = [self attributedTitleAtIndex:idx];
        if (idx) {
            if (stringWidth > weakSelf.rightTitleWidth) {
                NSMutableAttributedString *newAttrString = [string mutableCopy];//[[NSMutableAttributedString alloc]initWithAttributedString:string];
                NSRange range = NSMakeRange(0, newAttrString.length);
                UIFont *oldFont = [newAttrString attributesAtIndex:0 effectiveRange:&range][NSFontAttributeName];
                CGFloat newFontSize = weakSelf.rightTitleWidth/newAttrString.length * 1.5f;
                UIFont *newFont = [UIFont fontWithName:oldFont.fontName size:newFontSize];
                [newAttrString addAttribute:NSFontAttributeName value:newFont range:range];
                stringHeight = newAttrString.size.height;
                y = roundf(CGRectGetHeight(self.frame) / 2 - stringHeight / 2);
                weakSelf.rightTitleLayer.string = newAttrString;
            }else{
                weakSelf.rightTitleLayer.string = string;
            }
            x =  weakSelf.leftTitleWidth + (weakSelf.rightTitleWidth - stringWidth)/2.f ;
            rect = CGRectMake(x, y,  stringWidth, stringHeight);
            
            rect = CGRectMake(ceilf(rect.origin.x), ceilf(rect.origin.y), ceilf(rect.size.width), ceilf(rect.size.height));
            weakSelf.rightTitleLayer.frame = rect;
        }else{
            if (stringWidth > weakSelf.leftTitleWidth) {
                NSMutableAttributedString *newAttrString = [string mutableCopy];//[[NSMutableAttributedString alloc]initWithAttributedString:string];
                NSRange range = NSMakeRange(0, newAttrString.length);
                UIFont *oldFont = [newAttrString attributesAtIndex:0 effectiveRange:&range][NSFontAttributeName];
                CGFloat newFontSize = weakSelf.leftTitleWidth/newAttrString.length * 1.5f;
                UIFont *newFont = [UIFont fontWithName:oldFont.fontName size:newFontSize];
                [newAttrString addAttribute:NSFontAttributeName value:newFont range:range];
                stringHeight = newAttrString.size.height;
                y = roundf(CGRectGetHeight(self.frame) / 2 - stringHeight / 2);
                weakSelf.leftTitleLayer.string = newAttrString;
            }else{
                weakSelf.leftTitleLayer.string = string;
            }
            x = (weakSelf.leftTitleWidth - stringWidth)/2.f;
            rect = CGRectMake(x, y, stringWidth, stringHeight);
            rect = CGRectMake(ceilf(rect.origin.x), ceilf(rect.origin.y), ceilf(rect.size.width), ceilf(rect.size.height));
            weakSelf.leftTitleLayer.frame = rect;
        }
    }];
    self.selectionIndicatorLayer.frame = [self frameForSelectionIndicator];
}

///计算文字大小
- (CGSize)measureTitleAtIndex:(NSUInteger)index {
    if (index >= self.titles.count) {
        return CGSizeZero;
    }
    
    id title = self.titles[index];
    CGSize size = CGSizeZero;
    BOOL selected = (index == self.selectIndex) ? YES : NO;
    NSDictionary *titleAttrs = selected ? [self resultingSelectedTitleTextAttributes] : [self resultingTitleTextAttributes];
    size = [(NSString *)title sizeWithAttributes:titleAttrs];
    return CGRectIntegral((CGRect){CGPointZero, size}).size;
}

/// 获得显示字符串
- (NSAttributedString *)attributedTitleAtIndex:(NSUInteger)index {
    id title = self.titles[index];
    BOOL selected = (index == self.selectIndex) ? YES : NO;
    
    if ([title isKindOfClass:[NSAttributedString class]]) {
        return (NSAttributedString *)title;
    } else {
        NSDictionary *titleAttrs = selected ? [self resultingSelectedTitleTextAttributes] : [self resultingTitleTextAttributes];
        
        // the color should be cast to CGColor in order to avoid invalid context on iOS7
        UIColor *titleColor = titleAttrs[NSForegroundColorAttributeName];
        
        if (titleColor) {
            NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:titleAttrs];
            
            dict[NSForegroundColorAttributeName] = titleColor;
            
            titleAttrs = [NSDictionary dictionaryWithDictionary:dict];
        }
        
        return [[NSAttributedString alloc] initWithString:(NSString *)title attributes:titleAttrs];
    }
}

#pragma mark - Styling Support

- (void)updateWidth{
    __weak __typeof(self)weakSelf = self;
    [_titles enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        CGFloat stringWidth = [self measureTitleAtIndex:idx].width;
        if (idx) {
            weakSelf.rightTitleWidth = stringWidth + 1.5f * weakSelf.cornerRadius;
        }else{
            weakSelf.leftTitleWidth = stringWidth + 1.5f * weakSelf.cornerRadius;
        }
    }];
    [self setNeedsLayout];
    [self setNeedsDisplay];
}

- (NSDictionary *)resultingTitleTextAttributes {
    NSDictionary *defaults = @{
                               NSFontAttributeName : [UIFont systemFontOfSize:19.0f],
                               NSForegroundColorAttributeName : [UIColor blackColor],
                               };
    
    NSMutableDictionary *resultingAttrs = [NSMutableDictionary dictionaryWithDictionary:defaults];
    
    if (self.titleTextAttributes) {
        [resultingAttrs addEntriesFromDictionary:self.titleTextAttributes];
    }
    
    return [resultingAttrs copy];
}

- (NSDictionary *)resultingSelectedTitleTextAttributes {
    NSMutableDictionary *resultingAttrs = [NSMutableDictionary dictionaryWithDictionary:[self resultingTitleTextAttributes]];
    
    if (self.selectedTitleTextAttributes) {
        [resultingAttrs addEntriesFromDictionary:self.selectedTitleTextAttributes];
    }
    
    return [resultingAttrs copy];
}

@end
