//
//  GZCTableSection.h
//  GZCKit
//
//  Created by GuoZhongCheng on 2018/7/3.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class ASDisplayNode;
@interface GZCTableSection : NSObject

/// 行数
@property ( nonatomic, assign, readonly ) NSInteger numberOfRow;

/// items的数据源
@property ( nonatomic, strong ) NSArray *itemModels;

/// header标题(有的话)
@property ( nonatomic,   copy ) NSString *headerTitle;

/// footer标题(有的话)
@property ( nonatomic,   copy ) NSString *footerTitle;

/// header高度
@property ( nonatomic, assign ) CGFloat headerHeight;

/// footer高度
@property ( nonatomic, assign ) CGFloat footerHeight;

/// header的UIView视图(有的话)
@property ( nonatomic, strong ) UIView *headerView;

/// header的ASDisplyNode视图(有的话)
@property ( nonatomic, strong ) ASDisplayNode *headerNode;

/// footer的UIView视图(有的话)
@property ( nonatomic, strong ) UIView *footerView;

/// footer的ASDisplayNode视图(有的话)
@property ( nonatomic, strong ) ASDisplayNode *footerNode;

#pragma mark - 初始化方法
/// 直接创建
+ (instancetype)section;
/***
 根据title或者view进行初始化
 ***/
- (instancetype)initWithHeaderTitle:(NSString *)headerTitle;
- (instancetype)initWithFooterTitle:(NSString *)footerTitle;
- (instancetype)initWithHeaderTitle:(NSString *)headerTitle
                        footerTitle:(NSString *)footerTitle;

+ (instancetype)sectionWithHeaderTitle:(NSString *)headerTitle;
+ (instancetype)sectionWithFooterTitle:(NSString *)footerTitle;
+ (instancetype)sectionWithHeaderTitle:(NSString *)headerTitle
                           footerTitle:(NSString *)footerTitle;

- (instancetype)initWithHeaderView:(UIView *)headerView;
- (instancetype)initWithFooterView:(UIView *)footerView;
- (instancetype)initWithHeaderView:(UIView *)headerView
                        footerView:(UIView *)footerView;

+ (instancetype)sectionWithHeaderView:(UIView *)headerView;
+ (instancetype)sectionWithFooterView:(UIView *)footerView;
+ (instancetype)sectionWithHeaderView:(UIView *)headerView
                           footerView:(UIView *)footerView;

#pragma 增删改操作
/*** 新增元素 ***/
- (void)addItem:(id)itemModel;
- (void)addItemsFromArray:(NSArray *)array;

/*** 插入元素 ***/
- (void)insertItem:(id)item
           atIndex:(NSUInteger)index;
- (void)insertItems:(NSArray *)items
          atIndexes:(NSIndexSet *)indexes;

/*** 删除元素 ***/
- (void)removeLastItem;
- (void)removeAllItems;
- (void)removeItem:(id)item;
- (void)removeItemsInRange:(NSRange)range;
- (void)removeItemAtIndex:(NSUInteger)index;
- (void)removeItemsInArray:(NSArray *)array;
- (void)removeItem:(id)item inRange:(NSRange)range;
- (void)removeItemsAtIndexes:(NSIndexSet *)indexes;

/*** 替换元素 ***/
- (void)replaceItemAtIndex:(NSUInteger)index
                  withItem:(id)item;
- (void)replaceItemsAtIndexes:(NSIndexSet *)indexes
                    withItems:(NSArray *)items;
- (void)replaceItemsInRange:(NSRange)range
         withItemsFromArray:(NSArray *)array;
- (void)replaceItemsWithItemsFromArray:(NSArray *)array;

/*** 交换元素 ***/
- (void)exchangeItemAtIndex:(NSUInteger)idx1
            withItemAtIndex:(NSUInteger)idx2;

/*** 元素排序 ***/
- (void)sortItemsUsingFunction:(NSInteger (*)(id, id, void *))compare
                       context:(void *)context;
- (void)sortItemsUsingSelector:(SEL)comparator;

@end
