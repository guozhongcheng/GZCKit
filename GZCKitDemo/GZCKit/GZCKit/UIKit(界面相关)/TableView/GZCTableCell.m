//
//  GZCTableCell.m
//  GZCKit
//
//  Created by GuoZhongCheng on 2018/7/2.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//

#import "GZCTableCell.h"

@interface GZCTableCell()


@end

@implementation GZCTableCell

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setupCell];
        [self buildSubView];
        [self buildSubNode];
    }
    return self;
}

- (instancetype)initWithDelegate:(id<GZCTableCellDelegate>)delegate{
    if (self = [self init]) {
        self.delegate = delegate;
    }
    return self;
}

- (ASLayout *)layoutThatFits:(ASSizeRange)constrainedSize{
    return nil;
}

- (void)sendEventType:(NSInteger)eventType withObject:(id)objectInfo{
    if (self.delegate && [self.delegate respondsToSelector:@selector(gzcTableCell:eventType:object:)]) {
        [self.delegate gzcTableCell:self eventType:eventType object:objectInfo];
    }
}

- (void)addSubview:(UIView *)view{
    [self.view addSubview:view];
}

#pragma mark - 子类中需要重写的方法
- (void)setupCell{

}

- (void)buildSubNode{
    
}

- (void)buildSubView{
    
}

- (void)setSubViewLayout{
    
}


@end
