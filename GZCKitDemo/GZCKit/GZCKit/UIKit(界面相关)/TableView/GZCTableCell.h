//
//  GZCTableCell.h
//  GZCKit
//
//  Created by GuoZhongCheng on 2018/7/2.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//

#import <AsyncDisplayKit/ASCellNode.h>

@class GZCTableCell;

@protocol GZCTableCellDelegate <NSObject>

/**
 回调事件
 
 @param eventType 回调事件的type，可以与空间的tag对应，也可以自己定义
 @param objectInfo 回调事件带的参数
 @param cell 回调的事件的触发Cell
 */
- (void)gzcTableCell:(GZCTableCell *)cell
           eventType:(NSInteger)eventType
              object:(id)objectInfo;

@end

@interface GZCTableCell : ASCellNode

/// 代理
@property ( nonatomic, weak ) id <GZCTableCellDelegate> delegate;



/**
 初始化方法

 @param delegate 代理
 @return GZCTableCell
 */
- (instancetype)initWithDelegate:(id <GZCTableCellDelegate>)delegate;

/**
 转发出点击事件

 @param eventType 事件的type
 @param objectInfo 附带的参数
 */
- (void)sendEventType:(NSInteger)eventType
           withObject:(id)objectInfo;

/**
 添加子View的方法

 @param view subView
 */
- (void)addSubview:(UIView *)view;

#pragma mark - 以下是需要overwrite的方法,按需重写即可
#pragma mark - 基本参数设置
/**
 设置一些Cell的基本参数
 */
- (void)setupCell;

#pragma mark - 添加Node
/**
 添加子Node，该方法会在子线程调用，请勿在方法中添加或修改UI系列控件和事件，比如说setImage...
 */
- (void)buildSubNode;


/**
 设置子Node的布局

 @param constrainedSize size的范围（最大最小宽高，具体看定义）
 @return ASLayout对象，目前有6种，具体使用参考Demo:
            https://github.com/chaserr/ASDisplayKitLayoutSpecDemo
 */
- (ASLayout *)layoutThatFits:(ASSizeRange)constrainedSize;

#pragma mark - 添加UI视图
/**
 添加子View，该方法在主线程调用,在这个方法中初始化所有的子view
 */
- (void)buildSubView;

/**
 设置子View的显示（在这里设置自动布局、或者直接设置子view的frame）
 ****  注意：设置自动布局时，最底层的view为self.view  ****
 */
- (void)setSubViewLayout;

@end
