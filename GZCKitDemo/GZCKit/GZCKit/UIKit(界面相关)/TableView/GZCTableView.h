//
//  GZCTableView.h
//  GZCKit
//
//  Created by GuoZhongCheng on 2018/7/2.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AsyncDisplayKit/AsyncDisplayKit.h>
#import "GZCTableAdapter.h"
#import "GZCTableSection.h"

@class GZCTableView;
@protocol GZCTableViewDelegate

@optional

/**
 GZCGridView点击事件回调
 
 @param gridView GZCGridView对象
 @param cell GZCGridCell类型的Cell
 @param event GZCGridCell的event事件（多个按钮点击时用于区分不同事件）
 */
- (void)tableView:(GZCTableView *)gridView
  didSelectedCell:(GZCTableCell *)cell
            event:(id)event;

@end

@interface GZCTableView : NSObject

/// 代理
@property ( nonatomic, weak ) id<GZCTableViewDelegate> delegate;

/// 内容的边距
@property ( nonatomic, assign ) UIEdgeInsets contentEdgeInsets;

/// 行间距
@property ( nonatomic, assign ) CGFloat lineSpacing;

/**
 滚动方向,值为：
 ASScrollDirectionNone  = 0,
 ASScrollDirectionRight = 1 << 0,
 ASScrollDirectionLeft  = 1 << 1,
 ASScrollDirectionUp    = 1 << 2,
 ASScrollDirectionDown  = 1 << 3
 */
@property ( nonatomic, assign ) ASScrollDirection scrollDirection;

/// 注册适配器
@property ( nonatomic, strong ) NSDictionary <Class,GZCTableAdapter *> *registerAdapters;

/// 数据源，以section为单位进行划分，可以在section中设置对应的header和footer
@property ( nonatomic, strong ) NSMutableArray <GZCTableSection *> *sections;

/// 显示内容的主控件，这里是tableNode
@property ( nonatomic, strong, readonly ) ASTableNode *tableNode;

/**
 初始化方法
 
 @param frame frame
 @return GZCGridView对象
 */
+ (instancetype)tableViewWithFrame:(CGRect)frame
                          delegate:(id <GZCTableViewDelegate>)delegate
                          adapters:(NSDictionary <Class,GZCTableAdapter *>*)adapters;


- (void)addSectionWithModels:(NSArray *)sectionArray;

- (void)insertRowsWithModels:(id)models
                   toSection:(NSInteger)section;


- (void)reloadData;

@end
