//
//  GZCTableAdapter.m
//  GZCKit
//
//  Created by GuoZhongCheng on 2018/7/2.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//

#import "GZCTableAdapter.h"
#import "GZCConstants.h"

@interface GZCTableAdapter() <GZCTableCellDelegate>

/// cell与model的对应管理字典，key为model，value为ASCellNodeBlock
@property ( nonatomic, strong ) NSMutableDictionary *modelCellDictionary;

/// adapter对应的cell类名
@property ( atomic ) Class CellClass;

@end

@implementation GZCTableAdapter

- (instancetype)initWithCellClassNamed:(NSString *)cellClassName{
    self = [super init];
    if (self) {
        self.CellClass = NSClassFromString(cellClassName);
    }
    return self;
}

+ (instancetype)adapterWithCellClassNamed:(NSString *)cellClassName{
    return [[self alloc]initWithCellClassNamed:cellClassName];
}

- (ASCellNodeBlock)cellWithModel:(id)model{
    ASCellNodeBlock nodeBlock = self.modelCellDictionary[model];
    if (nodeBlock == nil) {
        _gzc_weakSelf();
        nodeBlock = ^ASCellNode *() {
            GZCTableCell *cellNode = [[weakSelf.CellClass alloc] initWithDelegate:weakSelf];
            [weakSelf updateCell:cellNode withModel:model];
            return cellNode;
        };
        self.modelCellDictionary[model] = nodeBlock;
    }else{
        GZCTableCell *cellNode = (GZCTableCell *)nodeBlock();
        [self updateCell:cellNode withModel:model];
    }
    return nodeBlock;
}

#pragma mark - Cell Delegate
- (void)gzcTableCell:(GZCTableCell *)cell eventType:(NSInteger)eventType object:(id)objectInfo{
    [self tableCell:cell eventType:eventType object:objectInfo];
}

#pragma mark - 子类中overwrite
- (void)tableCell:(GZCTableCell *)cell eventType:(NSInteger)type object:(id)objectInfo{
    
}

- (void)updateCell:(GZCTableCell *)cell withModel:(id)model{
    
}

@end
