//
//  GZCTableView.m
//  GZCKit
//
//  Created by GuoZhongCheng on 2018/7/2.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//

#import "GZCTableView.h"

@interface GZCTableView() <ASTableDelegate, ASTableDataSource>

@end

@implementation GZCTableView

- (instancetype)init
{
    self = [super init];
    if (self) {
        _tableNode = [[ASTableNode alloc]init];
        _tableNode.dataSource = self;
        _tableNode.delegate = self;
        _tableNode.view.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableNode.leadingScreensForBatching = 1.0;
    }
    return self;
}

+ (instancetype)tableViewWithFrame:(CGRect)frame
                          delegate:(id <GZCTableViewDelegate>)delegate
                          adapters:(NSDictionary <Class,GZCTableAdapter *>*)adapters{
    GZCTableView *gzcTableView = [[self alloc]init];
    gzcTableView.delegate = delegate;
    gzcTableView.registerAdapters = adapters;
    gzcTableView.tableNode.frame = frame;
    return gzcTableView;
}

- (void)dealloc{
    self.tableNode.delegate = nil;
    self.tableNode.dataSource = nil;
}

- (void)reloadRowsAtIndexPaths:(NSArray <NSIndexPath *> *)indexPaths
              withRowAnimation:(UITableViewRowAnimation)animation{
    [self.tableNode reloadRowsAtIndexPaths:indexPaths withRowAnimation:animation];
}

- (void)reloadData{
    [self.tableNode reloadData];
}

#pragma mark - TableView DataSource
- (NSInteger)tableNode:(ASTableNode *)tableNode numberOfRowsInSection:(NSInteger)section{
    if (self.sections.count > section) {
        return self.sections[section].numberOfRow;
    }
    return 0;
}

- (NSInteger)numberOfSectionsInTableNode:(ASTableNode *)tableNode{
    return self.sections.count;
}

- (ASCellNodeBlock)tableNode:(ASTableNode *)tableNode nodeBlockForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id model = self.sections[indexPath.section].itemModels[indexPath.row];
    Class key = [model class];
    GZCTableAdapter *adapter = _registerAdapters[key];
    return [adapter cellWithModel:model];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = self.sections[section].headerView;
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    GZCTableSection *tableViewSection = self.sections[section];
    if (tableViewSection.headerHeight != 0.f) {
        return tableViewSection.headerHeight;
    }
    if (tableViewSection.headerNode) {
        if (CGSizeEqualToSize(tableViewSection.headerNode.calculatedSize, CGSizeZero)) {
            [tableViewSection.headerNode layoutThatFits:ASSizeRangeMake(CGSizeMake(tableView.bounds.size.width, 0), CGSizeMake(tableView.bounds.size.width, CGFLOAT_MAX))];
        }
        return CGRectGetHeight(tableViewSection.headerNode.frame);
    }
    if (tableViewSection.headerView) {
        return CGRectGetHeight(tableViewSection.headerView.frame) ? : UITableViewAutomaticDimension;
    }
    if (tableViewSection.headerTitle) {
        CGFloat headerHeight = 0;
        CGFloat headerWidth = CGRectGetWidth(CGRectIntegral(tableView.bounds)) - 40.0f;
    
        CGSize headerRect = CGSizeMake(headerWidth, CGFLOAT_MAX);
        
        CGRect headerFrame = [tableViewSection.headerTitle boundingRectWithSize:headerRect
                                                                        options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                                                     attributes:@{ NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline] }
                                                                        context:nil];
        
        headerHeight = headerFrame.size.height;
        
        return headerHeight + (tableView.style == UITableViewStylePlain ? 20 : 12);
    }
    return UITableViewAutomaticDimension;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    GZCTableSection *tableViewSection = self.sections[section];
    if (tableViewSection.footerHeight!=0.0) {
        return tableViewSection.footerHeight;
    }
    if (tableViewSection.footerNode) {
        if (CGSizeEqualToSize(tableViewSection.footerNode.calculatedSize, CGSizeZero) ) {
            [tableViewSection.footerNode layoutThatFits:ASSizeRangeMake(CGSizeMake(tableView.bounds.size.width, 0), CGSizeMake(tableView.bounds.size.width, CGFLOAT_MAX))];
            tableViewSection.footerNode.frame = CGRectMake(0, 0, tableViewSection.footerNode.calculatedSize.width, tableViewSection.footerNode.calculatedSize.height);
        }
        return CGRectGetHeight(tableViewSection.footerNode.frame);
    }
    if (tableViewSection.footerView) {
        return CGRectGetHeight(tableViewSection.footerView.frame)? :UITableViewAutomaticDimension;
    }
    if (tableViewSection.footerTitle) {
        CGFloat footerHeight = 0;
        CGFloat footerWidth = CGRectGetWidth(CGRectIntegral(tableView.bounds)) - 40.0f; // 40 = 20pt horizontal padding on each side
        
        CGSize footerRect = CGSizeMake(footerWidth, CGFLOAT_MAX);
        
        CGRect footerFrame = [tableViewSection.footerTitle boundingRectWithSize:footerRect
                                                                        options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                                                     attributes:@{ NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleFootnote] }
                                                                        context:nil];
        
        footerHeight = footerFrame.size.height;
        
        return footerHeight + 10.0f;
    }
    return UITableViewAutomaticDimension;
}

@end
