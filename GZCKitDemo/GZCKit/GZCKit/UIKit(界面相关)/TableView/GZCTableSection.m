//
//  GZCTableSection.m
//  GZCKit
//
//  Created by GuoZhongCheng on 2018/7/3.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//

#import "GZCTableSection.h"

@interface GZCTableSection()

/// 保存section中的数据
@property ( nonatomic, strong ) NSMutableArray *mutableItems;

@end

@implementation GZCTableSection

#pragma mark - 初始化方法
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setDefaultValues];
    }
    return self;
}

- (void)setDefaultValues{
    _mutableItems = [NSMutableArray array];
    _headerHeight = 0.f;
    _footerHeight = 0.f;
}


+ (instancetype)section {
    return [[self alloc] init];
}

- (instancetype)initWithHeaderTitle:(NSString *)headerTitle {
    self = [super init];
    if (self) {
        [self setDefaultValues];
        self.headerTitle = headerTitle;
    }
    return self;
}

- (instancetype)initWithFooterTitle:(NSString *)footerTitle {
    self = [super init];
    if (self) {
        [self setDefaultValues];
        self.footerTitle = footerTitle;
    }
    return self;
}

- (instancetype)initWithHeaderTitle:(NSString *)headerTitle footerTitle:(NSString *)footerTitle {
    self = [super init];
    if (self) {
        [self setDefaultValues];
        self.headerTitle = headerTitle;
        self.footerTitle = footerTitle;
    }
    return self;
}

+ (instancetype)sectionWithHeaderTitle:(NSString *)headerTitle {
    return [[self alloc] initWithHeaderTitle:headerTitle];
}

+ (instancetype)sectionWithFooterTitle:(NSString *)footerTitle {
    return [[self alloc] initWithFooterTitle:footerTitle];
}

+ (instancetype)sectionWithHeaderTitle:(NSString *)headerTitle footerTitle:(NSString *)footerTitle {
    return [[self alloc] initWithHeaderTitle:headerTitle footerTitle:footerTitle];
}

- (instancetype)initWithHeaderView:(UIView *)headerView {
    self = [super init];
    if (self) {
        [self setDefaultValues];
        self.headerView = headerView;
    }
    return self;
}

- (instancetype)initWithFooterView:(UIView *)footerView {
    self = [super init];
    if (self) {
        [self setDefaultValues];
        self.footerView = footerView;
    }
    return self;
}

- (instancetype)initWithHeaderView:(UIView *)headerView footerView:(UIView *)footerView {
    self = [super init];
    if (self) {
        [self setDefaultValues];
        self.headerView  = headerView;
        self.footerView  = footerView;
    }
    return self;
}

+ (instancetype)sectionWithHeaderView:(UIView *)headerView {
    return [[self alloc] initWithHeaderView:headerView];
}

+ (instancetype)sectionWithFooterView:(UIView *)footerView {
    return [[self alloc] initWithFooterView:footerView];
}

+ (instancetype)sectionWithHeaderView:(UIView *)headerView footerView:(UIView *)footerView {
    return [[self alloc] initWithHeaderView:headerView footerView:footerView];
}

#pragma mark -
- (NSArray *)itemModels{
    return self.mutableItems;
}

- (NSInteger)numberOfRow{
    return self.mutableItems.count;
}

#pragma mark - 赠删改操作
/*** 新增元素 ***/
- (void)addItem:(id)itemModel{
    [self.mutableItems addObject:itemModel];
}
- (void)addItemsFromArray:(NSArray *)array{
    [self.mutableItems addObjectsFromArray:array];
}

/*** 插入元素 ***/
- (void)insertItem:(id)item
           atIndex:(NSUInteger)index{
    [self.mutableItems insertObject:item atIndex:index];
}

- (void)insertItems:(NSArray *)items
          atIndexes:(NSIndexSet *)indexes{
    [self.mutableItems insertObjects:items atIndexes:indexes];
}

/*** 删除元素 ***/
- (void)removeLastItem{
    [self.mutableItems removeLastObject];
}

- (void)removeAllItems{
    [self.mutableItems removeAllObjects];
}

- (void)removeItem:(id)item{
    [self.mutableItems removeObject:item];
}
    
- (void)removeItemsInRange:(NSRange)range{
    [self.mutableItems removeObjectsInRange:range];
}

- (void)removeItemAtIndex:(NSUInteger)index{
    [self.mutableItems removeObjectAtIndex:index];
}

- (void)removeItemsInArray:(NSArray *)array{
    [self.mutableItems removeObjectsInArray:array];
}

- (void)removeItem:(id)item inRange:(NSRange)range{
    [self.mutableItems removeObject:item inRange:range];
}

- (void)removeItemsAtIndexes:(NSIndexSet *)indexes{
    [self.mutableItems removeObjectsAtIndexes:indexes];
}


/*** 替换元素 ***/
- (void)replaceItemAtIndex:(NSUInteger)index
                  withItem:(id)item{
    [self.mutableItems replaceObjectAtIndex:index withObject:item];
}

- (void)replaceItemsAtIndexes:(NSIndexSet *)indexes
                    withItems:(NSArray *)items{
    [self.mutableItems replaceObjectsAtIndexes:indexes withObjects:items];
}

- (void)replaceItemsInRange:(NSRange)range
         withItemsFromArray:(NSArray *)array{
    [self.mutableItems replaceObjectsInRange:range withObjectsFromArray:array];
}
    
- (void)replaceItemsWithItemsFromArray:(NSArray *)array{
    [self.mutableItems removeAllObjects];
    [self.mutableItems addObjectsFromArray:array];
}

/*** 交换元素 ***/
- (void)exchangeItemAtIndex:(NSUInteger)idx1
            withItemAtIndex:(NSUInteger)idx2{
    [self.mutableItems exchangeObjectAtIndex:idx1 withObjectAtIndex:idx1];
}

/*** 元素排序 ***/
- (void)sortItemsUsingFunction:(NSInteger (*)(id, id, void *))compare
                       context:(void *)context{
    [self.mutableItems sortUsingFunction:compare context:context];
}

- (void)sortItemsUsingSelector:(SEL)comparator{
    [self.mutableItems sortedArrayUsingSelector:comparator];
}

@end
