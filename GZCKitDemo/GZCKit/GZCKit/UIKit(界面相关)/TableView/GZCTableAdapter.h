//
//  GZCTableAdapter.h
//  GZCKit
//
//  Created by GuoZhongCheng on 2018/7/2.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AsyncDisplayKit/AsyncDisplayKit.h>
#import "GZCTableCell.h"

@interface GZCTableAdapter : NSObject

/// 代理
@property ( nonatomic, weak ) id delegate;

/// 不要使用这个方法初始化
- (instancetype)init NS_UNAVAILABLE;

/// 初始化（根据对应Cell的类名进行初始化）
- (instancetype)initWithCellClassNamed:(NSString *)cellClassName NS_REQUIRES_SUPER;
+ (instancetype)adapterWithCellClassNamed:(NSString *)cellClassName NS_REQUIRES_SUPER;

/// 根据传入的model查找cell（不存在则创建）
- (ASCellNodeBlock)cellWithModel:(id)model;

#pragma mark - 需要overwrite的方法
/**
 cell中的点击事件,子类重写后在其中处理事件逻辑
 如果需要再回调给ViewController，调用方法

 @param cell 回调的事件的触发Cell
 @param type 回调事件的type，可以与空间的tag对应，也可以自己定义
 @param objectInfo 回调事件带的参数
 */
- (void)tableCell:(GZCTableCell *)cell
        eventType:(NSInteger)type
           object:(id)objectInfo;

/**
 model改变时更新cell

 @param cell cell
 @param model model
 */
- (void)updateCell:(GZCTableCell *)cell
         withModel:(id)model;

@end
