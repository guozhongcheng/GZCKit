//
//  NSString+GZCMatch.h
//  GZCKit
//
//  Created by GuoZhongCheng on 2018/7/26.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (GZCMatch)

#pragma mark - 校验
/// 是否符合手机号格式
- (BOOL)isPhoneNumber;
/** 是否是纯数字 */
- (BOOL)isNum;

#pragma mark - 格式化
/**
 数字过大用单位表示
 数量小于1K（千），则有多少显示多少
 数量大于等于1K（千）且小于 1 M（百万)
 数量大于等于1M，则以 M 为单位
 
 @param remain 保留小数点位数
 @return 转换后的数字字符串
 */
- (NSString *)changeAmountWithRemain:(NSInteger)remain;

/**
 每隔4个字符添加一个空格
 银行卡格式
 **/
+ (NSString *)dealWithString:(NSString *)number;

#pragma mark - 四则运算
- (NSDecimalNumber *)decimalWrapper;
- (NSDecimal)decimalStruct;

/// 比较运算（>,>=,<,<=,==,!=）结果
typedef BOOL (^GZCCompareBlock)(NSString *string);
/// 四则运算结果
typedef NSString *(^GZCCalculationBlock) (NSString *string);
/**
 四则运算结果
 @param string 被操作数
 @param scale 结果精度（小数位数）
 @param zeroPadding 当小数位数不足scale时是否需要补零
 */
typedef NSString *(^GZCCalRoundBlock) (NSString *string,NSInteger scale,BOOL zeroPadding);
/**
 小数位控制
 @param scale 结果精度（小数位数）
 @param zeroPadding 当小数位数不足scale时是否需要补零
 */
typedef NSString *(^GZCRoundBlock) (NSInteger scale,BOOL zeroPadding);

/***正常结果***/
/// ➕
- (GZCCalculationBlock)add;
/// ➖
- (GZCCalculationBlock)sub;
/// ✖️
- (GZCCalculationBlock)mul;
/// ➗
- (GZCCalculationBlock)div;

/***结果四舍五入***/
/// ➕
- (GZCCalRoundBlock)add_r;
/// ➖
- (GZCCalRoundBlock)sub_r;
/// ✖️
- (GZCCalRoundBlock)mul_r;
/// ➗
- (GZCCalRoundBlock)div_r;

/***结果向下舍入***/
/// ➕
- (GZCCalRoundBlock)add_d;
/// ➖
- (GZCCalRoundBlock)sub_d;
/// ✖️
- (GZCCalRoundBlock)mul_d;
/// ➗
- (GZCCalRoundBlock)div_d;

/***结果向上舍入***/
/// ➕
- (GZCCalRoundBlock)add_u;
/// ➖
- (GZCCalRoundBlock)sub_u;
/// ✖️
- (GZCCalRoundBlock)mul_u;
/// ➗
- (GZCCalRoundBlock)div_u;

/***比较运算***/
/// ＞
- (GZCCompareBlock)greater;
/// ＞=
- (GZCCompareBlock)greaterE;
/// <
- (GZCCompareBlock)less;
/// <=
- (GZCCompareBlock)lessE;
/// ==
- (GZCCompareBlock)equal;
/// !=
- (GZCCompareBlock)unEqual;

/***舍入***/
/// 四舍五入
- (GZCRoundBlock)r_plain;
/// 四舍五入去偶
- (GZCRoundBlock)r_bankers;
/// 向上舍入
- (GZCRoundBlock)r_up;
/// 向下舍入
- (GZCRoundBlock)r_down;

@end
