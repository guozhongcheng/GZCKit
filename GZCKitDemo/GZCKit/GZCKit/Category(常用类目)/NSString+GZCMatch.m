//
//  NSString+GZCMatch.m
//  GZCKit
//
//  Created by GuoZhongCheng on 2018/7/26.
//  Copyright © 2018年 ZhongCheng Guo. All rights reserved.
//

#import "NSString+GZCMatch.h"

@implementation NSString (GZCMatch)

#pragma mark - 校验
- (BOOL)isPhoneNumber {
    NSString *regex = @"[1][34578]\\d{9}";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [pred evaluateWithObject:self];
}

- (BOOL)isNum{
    NSString * string = [self stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
    if(string.length > 0) {
        return NO;
    }
    return YES;
}

#pragma mark - 格式化
- (NSString *)changeAmountWithRemain:(NSInteger)remain {
    if (self && ![self isEqualToString:@""]) {
        NSString *result = @"0";
        if (self.less(@"1000")) {
            result = self.r_down(remain,NO);
        } else if (self.greaterE(@"1000") && self.less(@"1000000")) {
            result = [NSString stringWithFormat:@"%@K",self.div(@"1000").r_down(remain,NO)];
        } else {
            result = [NSString stringWithFormat:@"%@M",self.div(@"1000000").r_down(remain,NO)];
        }
        return result;
    } else {
        return @"0";
    }
}

+ (NSString *)dealWithString:(NSString *)number
{
    NSString *doneTitle = @"";
    int count = 0;
    for (int i = 0; i < number.length; i++) {
        count++;
        doneTitle = [doneTitle stringByAppendingString:[number substringWithRange:NSMakeRange(i, 1)]];
        if (count == 4) {
            doneTitle = [NSString stringWithFormat:@"%@ ", doneTitle];
            count = 0;
        }
    }
    NSLog(@"%@", doneTitle);
    return doneTitle;
}

#pragma mark - 四则运算
- (NSDecimalNumber *)decimalWrapper{
    return [NSDecimalNumber decimalNumberWithString:self];
}

- (NSDecimal)decimalStruct{
    return self.decimalWrapper.decimalValue;
}

- (GZCCalculationBlock)add{
    return ^NSString *(NSString *string){
        NSString *result = nil;
        @try{
            result = [self.decimalWrapper decimalNumberByAdding:string.decimalWrapper].stringValue;
            return result;
        }@catch (NSException *exception){
            return @"0";
        }
    };
}

- (GZCCalculationBlock)sub{
    return ^NSString *(NSString *string){
        NSString *result = nil;
        @try{
            result = [self.decimalWrapper decimalNumberBySubtracting:string.decimalWrapper].stringValue;
            return result;
        }@catch (NSException *exception){
            return @"0";
        }
    };
}

- (GZCCalculationBlock)mul{
    return ^NSString *(NSString *string){
        NSString *result = nil;
        @try{
            result = [self.decimalWrapper decimalNumberByMultiplyingBy:string.decimalWrapper].stringValue;
            return result;
        }@catch (NSException *exception){
            return @"0";
        }
    };
}

- (GZCCalculationBlock)div{
    return ^NSString *(NSString *string){
        NSString *result = nil;
        @try{
            result = [self.decimalWrapper decimalNumberByDividingBy:string.decimalWrapper].stringValue;
            return result;
        }@catch (NSException *exception){
            return @"0";
        }
    };
}

- (GZCCalRoundBlock)add_r{
    return ^NSString *(NSString *string, NSInteger scale,BOOL zeroPadding){
        NSString *result = nil;
        @try{
            result = [self.decimalWrapper decimalNumberByAdding:string.decimalWrapper].stringValue;
            return result.r_plain(scale,zeroPadding);
        }@catch (NSException *exception){
            return @"0";
        }
    };
}

- (GZCCalRoundBlock)sub_r{
    return ^NSString *(NSString *string, NSInteger scale,BOOL zeroPadding){
        NSString *result = nil;
        @try{
            result = [self.decimalWrapper decimalNumberBySubtracting:string.decimalWrapper].stringValue;
            return result.r_plain(scale,zeroPadding);
        }@catch (NSException *exception){
            return @"0";
        }
    };
}

- (GZCCalRoundBlock)mul_r{
    return ^NSString *(NSString *string, NSInteger scale,BOOL zeroPadding){
        NSString *result = nil;
        @try{
            result = [self.decimalWrapper decimalNumberByMultiplyingBy:string.decimalWrapper].stringValue;
            return result.r_plain(scale,zeroPadding);
        }@catch (NSException *exception){
            return @"0";
        }
    };
}

- (GZCCalRoundBlock)div_r{
    return ^NSString *(NSString *string, NSInteger scale,BOOL zeroPadding){
        NSString *result = nil;
        @try{
            result = [self.decimalWrapper decimalNumberByDividingBy:string.decimalWrapper].stringValue;
            return result.r_plain(scale,zeroPadding);
        }@catch (NSException *exception){
            return @"0";
        }
    };
}

- (GZCCalRoundBlock)add_d{
    return ^NSString *(NSString *string, NSInteger scale,BOOL zeroPadding){
        NSString *result = nil;
        @try{
            result = [self.decimalWrapper decimalNumberByAdding:string.decimalWrapper].stringValue;
            return result.r_down(scale,zeroPadding);
        }@catch (NSException *exception){
            return @"0";
        }
    };
}

- (GZCCalRoundBlock)sub_d{
    return ^NSString *(NSString *string, NSInteger scale,BOOL zeroPadding){
        NSString *result = nil;
        @try{
            result = [self.decimalWrapper decimalNumberBySubtracting:string.decimalWrapper].stringValue;
            return result.r_down(scale,zeroPadding);
        }@catch (NSException *exception){
            return @"0";
        }
    };
}

- (GZCCalRoundBlock)mul_d{
    return ^NSString *(NSString *string, NSInteger scale,BOOL zeroPadding){
        NSString *result = nil;
        @try{
            result = [self.decimalWrapper decimalNumberByMultiplyingBy:string.decimalWrapper].stringValue;
            return result.r_down(scale,zeroPadding);
        }@catch (NSException *exception){
            return @"0";
        }
    };
}

- (GZCCalRoundBlock)div_d{
    return ^NSString *(NSString *string, NSInteger scale,BOOL zeroPadding){
        NSString *result = nil;
        @try{
            result = [self.decimalWrapper decimalNumberByDividingBy:string.decimalWrapper].stringValue;
            return result.r_down(scale,zeroPadding);
        }@catch (NSException *exception){
            return @"0";
        }
    };
}

- (GZCCalRoundBlock)add_u{
    return ^NSString *(NSString *string, NSInteger scale,BOOL zeroPadding){
        NSString *result = nil;
        @try{
            result = [self.decimalWrapper decimalNumberByAdding:string.decimalWrapper].stringValue;
            return result.r_up(scale,zeroPadding);
        }@catch (NSException *exception){
            return @"0";
        }
    };
}

- (GZCCalRoundBlock)sub_u{
    return ^NSString *(NSString *string, NSInteger scale,BOOL zeroPadding){
        NSString *result = nil;
        @try{
            result = [self.decimalWrapper decimalNumberBySubtracting:string.decimalWrapper].stringValue;
            return result.r_up(scale,zeroPadding);
        }@catch (NSException *exception){
            return @"0";
        }
    };
}

- (GZCCalRoundBlock)mul_u{
    return ^NSString *(NSString *string, NSInteger scale,BOOL zeroPadding){
        NSString *result = nil;
        @try{
            result = [self.decimalWrapper decimalNumberByMultiplyingBy:string.decimalWrapper].stringValue;
            return result.r_up(scale,zeroPadding);
        }@catch (NSException *exception){
            return @"0";
        }
    };
}

- (GZCCalRoundBlock)div_u{
    return ^NSString *(NSString *string, NSInteger scale,BOOL zeroPadding){
        NSString *result = nil;
        @try{
            result = [self.decimalWrapper decimalNumberByDividingBy:string.decimalWrapper].stringValue;
            return result.r_up(scale,zeroPadding);
        }@catch (NSException *exception){
            return @"0";
        }
    };
}

- (GZCRoundBlock)r_plain{
    return ^NSString *(NSInteger scale,BOOL zeroPadding){
        return [self roundWithScale:scale zeroPadding:zeroPadding roundModel:NSRoundPlain];
    };
}

- (GZCRoundBlock)r_bankers{
    return ^NSString *(NSInteger scale,BOOL zeroPadding){
        return [self roundWithScale:scale zeroPadding:zeroPadding roundModel:NSRoundBankers];
    };
}

- (GZCRoundBlock)r_down{
    return ^NSString *(NSInteger scale,BOOL zeroPadding){
        return [self roundWithScale:scale zeroPadding:zeroPadding roundModel:NSRoundDown];
    };
}

- (GZCRoundBlock)r_up{
    return ^NSString *(NSInteger scale,BOOL zeroPadding){
        return [self roundWithScale:scale zeroPadding:zeroPadding roundModel:NSRoundUp];
    };
}

- (GZCCompareBlock)greater{
    return ^BOOL (NSString *string){
        return [self.decimalWrapper compare:string.decimalWrapper] == NSOrderedDescending;
    };
}

- (GZCCompareBlock)greaterE{
    return ^BOOL (NSString *string){
        NSComparisonResult rel = [self.decimalWrapper compare:string.decimalWrapper];
        return rel == NSOrderedDescending || rel == NSOrderedSame;
    };
}

- (GZCCompareBlock)less{
    return ^BOOL (NSString *string){
        return [self.decimalWrapper compare:string.decimalWrapper] == NSOrderedAscending;
    };
}

- (GZCCompareBlock)lessE{
    return ^BOOL (NSString *string){
        NSComparisonResult rel = [self.decimalWrapper compare:string.decimalWrapper];
        return rel == NSOrderedAscending || rel == NSOrderedSame;
    };
}

- (GZCCompareBlock)equal{
    return ^BOOL (NSString *string){
        return [self.decimalWrapper compare:string.decimalWrapper] == NSOrderedSame;
    };
}

- (GZCCompareBlock)unEqual{
    return ^BOOL (NSString *string){
        return [self.decimalWrapper compare:string.decimalWrapper] != NSOrderedSame;
    };
}

- (NSString *)roundWithScale:(NSInteger)scale
                 zeroPadding:(BOOL)zeroPadding
                  roundModel:(NSRoundingMode)roundModel{
    NSDecimal result;
    NSDecimal origin = self.decimalStruct;
    NSDecimalRound(&result, &origin, scale, roundModel);
    NSString *resultStr = [NSDecimalNumber decimalNumberWithDecimal:result].stringValue;
    if (zeroPadding) {
        NSRange decimalRange = [resultStr rangeOfString:@"."];
        NSInteger paddingDigit = 0;
        NSMutableString *temStr = [NSMutableString stringWithString:resultStr];
        if (decimalRange.length == 1) {
            paddingDigit = scale - (resultStr.length - decimalRange.location - 1);
        }else{
            [temStr appendString:@"."];
            paddingDigit = scale;
        }
        while (paddingDigit) {
            [temStr appendString:@"0"];
            paddingDigit--;
        }
        resultStr = [temStr copy];
    }
    return resultStr;
}
@end
